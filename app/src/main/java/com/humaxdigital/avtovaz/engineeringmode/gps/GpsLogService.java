package com.humaxdigital.avtovaz.engineeringmode.gps;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.location.OnNmeaMessageListener;
import android.os.Binder;
import android.os.IBinder;

import com.humaxdigital.avtovaz.engineeringmode.Utils;
import com.humaxdigital.avtovaz.engineeringmode.gps.nmea.GnssStatus;
import com.humaxdigital.avtovaz.engineeringmode.gps.nmea.NmeaParser;

import java.util.ArrayList;

public class GpsLogService extends Service implements OnNmeaMessageListener {
    private final IBinder mBinder = new LocalBinder();
    private ArrayList<GpsLogServiceCallback> callbacks;

    private LocationManager locationManager;
    private NmeaParser nmeaParser;
    private GnssStatus gnssStatus;

    @Override
    public void onCreate() {
        super.onCreate();

        callbacks = new ArrayList<>();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            locationManager.addNmeaListener(this);
            nmeaParser = new NmeaParser();
            gnssStatus = nmeaParser.getGnssStatus();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy() {
        if (locationManager != null) {
            locationManager.removeNmeaListener(this);
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onNmeaMessage(String message, long timestamp) {
        nmeaParser.parseNmeaSentence(message);
        for (GpsLogServiceCallback callback : callbacks) {
            callback.onEvent(message);
        }
    }

    public void registerCallback(GpsLogServiceCallback callback) {
        Utils.log("Add " + callback);
        callbacks.add(callback);
    }

    public void unregisterCallback(GpsLogServiceCallback callback) {
        Utils.log("Remove " + callback);
        callbacks.remove(callback);
    }

    public class LocalBinder extends Binder {
        GpsLogService getService() {
            return GpsLogService.this;
        }

        GnssStatus getGnssStatus() {
            return GpsLogService.this.gnssStatus;
        }
    }

    public interface GpsLogServiceCallback {
        void onEvent(String message);
    }
}
