package com.humaxdigital.avtovaz.engineeringmode.audio;

import android.os.Bundle;
import android.os.SystemProperties;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

public class AudioRecordingFragment extends Fragment {
    private Switch swMicRec;
    private Switch swSpkRec;

    public AudioRecordingFragment() {
        // Required empty public constructor
    }

    static AudioRecordingFragment newInstance() {
        return new AudioRecordingFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_audio_recording, container, false);

        //
        swMicRec = view.findViewById(R.id.swMicRec);
        view.findViewById(R.id.swMicRec).setOnClickListener(v -> {
            try {
                SystemProperties.set("vendor.audio.dump.in", swMicRec.isChecked() ? "true" : "false");
            } catch (Exception ex) {
                Utils.log(ex.toString());
                Toast.makeText(App.getInstance().getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
            }
            swMicRec.setChecked(SystemProperties.get("vendor.audio.dump.in").equals("true"));
        });

        //
        swSpkRec = view.findViewById(R.id.swSpkRec);
        view.findViewById(R.id.swSpkRec).setOnClickListener(v -> {
            try {
                SystemProperties.set("vendor.audio.dump.out", swSpkRec.isChecked() ? "true" : "false");
            } catch (Exception ex) {
                Utils.log(ex.toString());
                Toast.makeText(App.getInstance().getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
            }
            swSpkRec.setChecked(SystemProperties.get("vendor.audio.dump.out").equals("true"));
        });
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        // read current settings and update UI
        swMicRec.setChecked(SystemProperties.get("vendor.audio.dump.in").equals("true"));
        swSpkRec.setChecked(SystemProperties.get("vendor.audio.dump.out").equals("true"));
    }
}
