package com.humaxdigital.avtovaz.engineeringmode.can;

import androidx.annotation.Nullable;

public class CanProperty {
    int mPropertyId;
    int mAreaId;
    String mPropertyName;
    Object mValue;

    CanProperty(int id, int area, String name, Object value) {
        mPropertyId = id;
        mAreaId = area;
        mPropertyName = name;
        mValue = value;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof CanProperty) {
            return (mPropertyId == ((CanProperty) obj).mPropertyId && mAreaId == ((CanProperty) obj).mAreaId);
        }
        return false;
    }
}
