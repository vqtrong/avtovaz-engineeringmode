package com.humaxdigital.avtovaz.engineeringmode.eth;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.humaxdigital.avtovaz.engineeringmode.ListMenuAdapter;
import com.humaxdigital.avtovaz.engineeringmode.R;

public class EthernetModeActivity extends AppCompatActivity {
    private String[] strMenuItems;

    private ListView listMenu;
    private EthernetInfoFragment ethernetInfoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ethernet_mode);

        strMenuItems = getResources().getStringArray(R.array.ethernet_mode_menu);

        listMenu = findViewById(R.id.listMenu);
        listMenu.setAdapter(new ListMenuAdapter(this.getLayoutInflater(), strMenuItems, true, false));
        listMenu.setOnItemClickListener((adapterView, view, i, l) -> showDetail(strMenuItems[i]));

        ethernetInfoFragment = EthernetInfoFragment.newInstance();

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        // show default
        listMenu.setItemChecked(0, true);
        showDetail(strMenuItems[0]);
    }

    private void showDetail(String title) {
        if (title.equals(getString(R.string.STR_ETHERNET_INFO))) {
            replaceFragment(ethernetInfoFragment);
        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragDetail, fragment);
        fragmentTransaction.commit();
    }
}
