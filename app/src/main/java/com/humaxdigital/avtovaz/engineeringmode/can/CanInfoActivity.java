package com.humaxdigital.avtovaz.engineeringmode.can;

import android.car.Car;
import android.car.VehicleAreaDoor;
import android.car.VehicleAreaType;
import android.car.hardware.CarPropertyValue;
import android.car.hardware.CarSensorEvent;
import android.car.hardware.CarSensorManager;
import android.car.hardware.CarVendorExtensionManager;
import android.car.hardware.cabin.CarCabinManager;
import android.car.hardware.hvac.CarHvacManager;
import android.hardware.automotive.vehicle.V2_0.VehicleProperty;
import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Supplier;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Integer.toHexString;

public class CanInfoActivity extends AppCompatActivity {
    private CarSensorManager mSensorManager;
    private CarCabinManager mCabinManager;
    private CarHvacManager mHvacManager;
    private CarVendorExtensionManager mCarVendorExtensionManager;

    private final CarSensorManager.OnSensorChangedListener mOnSensorChangedListener =
            new CarSensorManager.OnSensorChangedListener() {
                @Override
                public void onSensorChanged(CarSensorEvent event) {
                    switch (event.sensorType) {
                        case CarSensorManager.SENSOR_TYPE_CAR_SPEED:
                            Utils.log("PropId : " + VehicleProperty.toString(event.sensorType) + " / Value : " + event.getCarSpeedData(null).carSpeed);
                            updatePropertyItem(CarSensorManager.SENSOR_TYPE_CAR_SPEED, -1, event.getCarSpeedData(null).carSpeed);
                            break;

                        case CarSensorManager.SENSOR_TYPE_FUEL_LEVEL:
                            Utils.log("PropId : " + VehicleProperty.toString(event.sensorType) + " / Value : " + event.getFuelLevelData(null).level);
                            updatePropertyItem(CarSensorManager.SENSOR_TYPE_FUEL_LEVEL, -1, event.getFuelLevelData(null).level);
                            break;

                        case CarSensorManager.SENSOR_TYPE_FUEL_LEVEL_LOW:
                            Utils.log("PropId : " + VehicleProperty.toString(event.sensorType) + " / Value : " + event.getFuelLevelLowData(null).isFuelLevelLow);
                            updatePropertyItem(CarSensorManager.SENSOR_TYPE_FUEL_LEVEL_LOW, -1, event.getFuelLevelLowData(null).isFuelLevelLow);
                            break;

                        default:
                            break;
                    }
                }
            };

    private String getVehicleAreaDoorString(int area) {
        switch (area) {
            case VehicleAreaDoor.DOOR_ROW_1_RIGHT:
                return "DOOR_ROW_1_RIGHT";
            case VehicleAreaDoor.DOOR_ROW_1_LEFT:
                return "DOOR_ROW_1_LEFT";
            case VehicleAreaDoor.DOOR_ROW_2_RIGHT | VehicleAreaDoor.DOOR_ROW_2_LEFT:
                return "DOOR_ROW_2_RIGHT | DOOR_ROW_2_LEFT";
            case VehicleAreaDoor.DOOR_HOOD:
                return "DOOR_HOOD";
            case VehicleAreaDoor.DOOR_REAR:
                return "DOOR_REAR";
            default:
                return "0x" + Integer.toHexString(area);
        }
    }

    private String getDoorPosDescription(int status) {
        String valueString =
                status == 0 ? "Unknown" :
                        status == 1 ? "Close" :
                                status == 2 ? "Open" :
                                        "Not used";
        return valueString + " (" + status + ")";
    }

    private final CarCabinManager.CarCabinEventCallback mCabinCallback =
            new CarCabinManager.CarCabinEventCallback() {
                @Override
                public void onChangeEvent(final CarPropertyValue value) {
                    int id = value.getPropertyId();
                    int area = value.getAreaId();

                    switch (id) {
                        case CarCabinManager.ID_DOOR_POS: {
                            int status = (int) value.getValue();
                            Utils.log("PropId : " + VehicleProperty.toString(id) + ",    AreaId : " + getVehicleAreaDoorString(area) + "(" + area + ") / Value : " + status);

                            if ((area & VehicleAreaDoor.DOOR_REAR) > 0) {
                                updatePropertyItem(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_REAR, getDoorPosDescription(status));
                            }
                            if ((area & VehicleAreaDoor.DOOR_HOOD) > 0) {
                                updatePropertyItem(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_HOOD, getDoorPosDescription(status));
                            }
                            if ((area & VehicleAreaDoor.DOOR_ROW_1_LEFT) > 0) {
                                updatePropertyItem(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_1_LEFT, getDoorPosDescription(status));
                            }
                            if ((area & VehicleAreaDoor.DOOR_ROW_1_RIGHT) > 0) {
                                updatePropertyItem(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_1_RIGHT, getDoorPosDescription(status));
                            }
                            if ((area & VehicleAreaDoor.DOOR_ROW_2_LEFT) > 0) {
                                updatePropertyItem(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_2_LEFT, getDoorPosDescription(status));
                            }
                            if ((area & VehicleAreaDoor.DOOR_ROW_2_RIGHT) > 0) {
                                updatePropertyItem(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_2_RIGHT, getDoorPosDescription(status));
                            }
                            if ((area & VehicleAreaDoor.DOOR_ROW_3_LEFT) > 0) {
                                updatePropertyItem(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_3_LEFT, getDoorPosDescription(status));
                            }
                            if ((area & VehicleAreaDoor.DOOR_ROW_3_RIGHT) > 0) {
                                updatePropertyItem(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_3_RIGHT, getDoorPosDescription(status));
                            }
                            break;
                        }

                        default:
                            break;
                    }
                }

                @Override
                public void onErrorEvent(final int propertyId, final int zone) {
                    Utils.log("Error:  propertyId=0x" + toHexString(propertyId) + ", zone=0x" + toHexString(zone));
                }
            };

    private String getOutsideAirTempDescription(float temp) {
        String valueString = (temp >= -40.0f && temp <= 214.0f) ? "Valid" : "Invalid";
        return "" + temp + " (" + valueString + ")";
    }

    private final CarHvacManager.CarHvacEventCallback mHvacCallback =
            new CarHvacManager.CarHvacEventCallback() {
                @Override
                public void onChangeEvent(final CarPropertyValue value) {
                    int id = value.getPropertyId();
                    int area = value.getAreaId();

                    switch (id) {
                        case CarHvacManager.ID_OUTSIDE_AIR_TEMP: {
                            float temp = (float) value.getValue();
                            Utils.log("PropId : " + VehicleProperty.toString(value.getPropertyId()) + ",    AreaId : " + Integer.toHexString(area) + " / Value : " + temp);
                            updatePropertyItem(CarHvacManager.ID_OUTSIDE_AIR_TEMP, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, getOutsideAirTempDescription(temp));
                            break;
                        }

                        default:
                            break;
                    }
                }

                @Override
                public void onErrorEvent(final int propertyId, final int zone) {
                    Utils.log("Error:  propertyId=0x" + toHexString(propertyId) + ", zone=0x" + toHexString(zone));
                }
            };

    private String getAlarmStatusDescription(int status) {
        String valueString = "Unknown";
        switch (status) {
            case 0:
                valueString = "Unguarded, Normal";
                break;

            case 1:
                valueString = "Unguarded, after alarm";
                break;

            case 2:
                valueString = "Unguarded, wait for door";
                break;

            case 3:
                valueString = "Guarded, Normal";
                break;

            case 4:
                valueString = "Guarded, open perimeter";
                break;

            case 5:
                valueString = "Guarded, Setting delay";
                break;

            case 6:
                valueString = "Guarded, w/o trunk";
                break;

            case 7:
                valueString = "Guarded, Alarm";
                break;
        }

        return valueString + " (" + status + ")";
    }

    private String getVehicleStatusDescription(int status) {
        String valueString = "Unknown";
        switch (status) {
            case 0:
                valueString = "Sleeping";
                break;

            case 1:
                valueString = "Technical Wakeup";
                break;

            case 2:
                valueString = "CutOff Pending";
                break;

            case 3:
                valueString = "Battery Temporary Level";
                break;

            case 4:
                valueString = "Accessory Level";
                break;

            case 5:
                valueString = "Ignition Level";
                break;

            case 6:
                valueString = "Starting In Progress";
                break;

            case 7:
                valueString = "Engine Running";
                break;

            case 8:
                valueString = "Auto Start";
                break;

            case 9:
                valueString = "Engine System Stop";
                break;
        }

        return valueString + " (" + status + ")";
    }

    private String getRearGearEngagedDescription(int status) {
        String valueString = "Unknown";
        switch (status) {
            case 0:
                valueString = "Not used";
                break;

            case 1:
                valueString = "Not Engaged";
                break;

            case 2:
                valueString = "Engaged";
                break;
        }

        return valueString + " (" + status + ")";
    }

    private String getParkingBrakeDescription(int status) {
        String valueString = "Unknown";
        switch (status) {
            case 0:
                valueString = "Not used";
                break;

            case 1:
                valueString = "Not Applied";
                break;

            case 2:
                valueString = "Applied";
                break;
        }

        return valueString + " (" + status + ")";
    }

    private final CarVendorExtensionManager.CarVendorExtensionCallback mCarVendorExtensionCallback =
            new CarVendorExtensionManager.CarVendorExtensionCallback() {
                @Override
                public void onChangeEvent(final CarPropertyValue value) {
                    int id = value.getPropertyId();
                    int area = value.getAreaId();

                    switch (id) {
                        case CarVendorExtensionManager.VENDOR_CANRX_ALARM_STATUS: {
                            int status = (int) value.getValue();
                            Utils.log("PropId=" + VehicleProperty.toString(id) + ", AreaId=0x" + Integer.toHexString(area) + ", Value=" + status);
                            updatePropertyItem(CarVendorExtensionManager.VENDOR_CANRX_ALARM_STATUS, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, getAlarmStatusDescription(status));
                            break;
                        }

                        case CarVendorExtensionManager.VENDOR_CANRX_VEHICLE_STATUS: {
                            int status = (int) value.getValue();
                            Utils.log("PropId=" + VehicleProperty.toString(id) + ", AreaId=0x" + Integer.toHexString(area) + ", Value=" + status);
                            updatePropertyItem(CarVendorExtensionManager.VENDOR_CANRX_VEHICLE_STATUS, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, getVehicleStatusDescription(status));
                            break;
                        }

                        case CarVendorExtensionManager.VENDOR_CANRX_REAR_GEAR_ENGAGED: {
                            int status = (int) value.getValue();
                            Utils.log("PropId=" + VehicleProperty.toString(id) + ", AreaId=0x" + Integer.toHexString(area) + ", Value=" + status);
                            updatePropertyItem(CarVendorExtensionManager.VENDOR_CANRX_REAR_GEAR_ENGAGED, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, getRearGearEngagedDescription(status));
                            break;
                        }

                        case CarVendorExtensionManager.VENDOR_CANRX_PARKING_BRAKE_ON: {
                            int status = (int) value.getValue();
                            Utils.log("PropId=" + VehicleProperty.toString(id) + ", AreaId=0x" + Integer.toHexString(area) + ", Value=" + status);
                            updatePropertyItem(CarVendorExtensionManager.VENDOR_CANRX_PARKING_BRAKE_ON, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, getParkingBrakeDescription(status));
                            break;
                        }

                        case CarVendorExtensionManager.VENDOR_CANRX_SAS: {
                            int status = (int) value.getValue();
                            Utils.log("PropId=" + VehicleProperty.toString(id) + ", AreaId=0x" + Integer.toHexString(area) + ", Value=" + status);
                            updatePropertyItem(CarVendorExtensionManager.VENDOR_CANRX_SAS, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, getParkingBrakeDescription(status));
                            break;
                        }

                        case CarVendorExtensionManager.VENDOR_CANRX_SAS_OFFSET: {
                            int status = (int) value.getValue();
                            Utils.log("PropId=" + VehicleProperty.toString(id) + ", AreaId=0x" + Integer.toHexString(area) + ", Value=" + status);
                            updatePropertyItem(CarVendorExtensionManager.VENDOR_CANRX_SAS_OFFSET, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, status);
                            break;
                        }

                        default:
                            break;
                    }
                }

                @Override
                public void onErrorEvent(final int propertyId, final int zone) {
                    Utils.log("Error:  propertyId=0x" + toHexString(propertyId) + ", zone=0x" + toHexString(zone));
                }
            };

    private ExpandableListView listPropertyGroups;
    private ListPropertyAdapter listPropertyAdapter;
    private HashMap<String, ArrayList<CanProperty>> mCanPropertyList;

    private void updatePropertyItem(int id, int area, Object value) {
        for (HashMap.Entry<String, ArrayList<CanProperty>> entry : mCanPropertyList.entrySet()) {
            ArrayList<CanProperty> properties = entry.getValue();
            for (CanProperty property : properties) {
                if (property.mPropertyId == id && property.mAreaId == area) {
                    property.mValue = value;
                    listPropertyAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_can_info);

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        listPropertyGroups = findViewById(R.id.listPropertyGroups);

        // Connect to Car Services
        Car car = App.getInstance().getCar();
        if (car != null) {
            try {
                mSensorManager = (CarSensorManager) car.getCarManager(android.car.Car.SENSOR_SERVICE);
                mCabinManager = (CarCabinManager) car.getCarManager(android.car.Car.CABIN_SERVICE);
                mHvacManager = (CarHvacManager) car.getCarManager(android.car.Car.HVAC_SERVICE);
                mCarVendorExtensionManager = (CarVendorExtensionManager) car.getCarManager(android.car.Car.VENDOR_EXTENSION_SERVICE);
            } catch (Exception ex) {
                Utils.log(ex.toString());
            }
        }

        // Make list of under monitoring CAN properties
        mCanPropertyList = new HashMap<>();

        // SENSORS //
        ArrayList<CanProperty> sensors = new ArrayList<>();
        sensors.add(new CanProperty(CarSensorManager.SENSOR_TYPE_CAR_SPEED, -1, "Speed",
                ((Supplier<Object>) () -> {
                    try {
                        return mSensorManager.getLatestSensorEvent(CarSensorManager.SENSOR_TYPE_FUEL_LEVEL).getFuelLevelData(null).level;
                    } catch (Exception ex) {
                        return 0f;
                    }
                }).get()));

        sensors.add(new CanProperty(CarSensorManager.SENSOR_TYPE_FUEL_LEVEL, -1, "Fuel Level",
                ((Supplier<Object>) () -> {
                    try {
                        return mSensorManager.getLatestSensorEvent(CarSensorManager.SENSOR_TYPE_FUEL_LEVEL).getFuelLevelData(null).level;
                    } catch (Exception ex) {
                        return 0f;
                    }
                }).get()));

        sensors.add(new CanProperty(CarSensorManager.SENSOR_TYPE_FUEL_LEVEL_LOW, -1, "Fuel Low",
                ((Supplier<Object>) () -> {
                    try {
                        return mSensorManager.getLatestSensorEvent(CarSensorManager.SENSOR_TYPE_FUEL_LEVEL_LOW).getFuelLevelLowData(null).isFuelLevelLow;
                    } catch (Exception ex) {
                        return false;
                    }
                }).get()));

        mCanPropertyList.put("Sensors", sensors);

        // CABIN //
        ArrayList<CanProperty> cabin = new ArrayList<>();
        cabin.add(new CanProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_REAR, "Tail Gate",
                getDoorPosDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCabinManager.getIntProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_REAR);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        cabin.add(new CanProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_HOOD, "Hood",
                getDoorPosDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCabinManager.getIntProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_HOOD);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        cabin.add(new CanProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_1_LEFT, "Driver Door",
                getDoorPosDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCabinManager.getIntProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_1_LEFT);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        cabin.add(new CanProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_1_RIGHT, "Passenger Door",
                getDoorPosDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCabinManager.getIntProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_1_RIGHT);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        cabin.add(new CanProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_2_LEFT, "Rear Left Door",
                getDoorPosDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCabinManager.getIntProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_2_LEFT);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        cabin.add(new CanProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_2_RIGHT, "Rear Right Door",
                getDoorPosDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCabinManager.getIntProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_2_RIGHT);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        cabin.add(new CanProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_3_LEFT, "Extra Rear Left Door",
                getDoorPosDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCabinManager.getIntProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_3_LEFT);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        cabin.add(new CanProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_3_RIGHT, "Extra Rear Right Door",
                getDoorPosDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCabinManager.getIntProperty(CarCabinManager.ID_DOOR_POS, VehicleAreaDoor.DOOR_ROW_3_RIGHT);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        mCanPropertyList.put("Cabin", cabin);

        // HVAC //
        ArrayList<CanProperty> hvac = new ArrayList<>();
        hvac.add(new CanProperty(CarHvacManager.ID_OUTSIDE_AIR_TEMP, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, "Outside Air Temperature",
                getOutsideAirTempDescription((float)
                        ((Supplier<Object>) () -> {
                            try {
                                return mHvacManager.getFloatProperty(CarHvacManager.ID_OUTSIDE_AIR_TEMP, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL);
                            } catch (Exception ex) {
                                return -100f;
                            }
                        }).get())));

        mCanPropertyList.put("HVAC", hvac);

        // CANRX //
        ArrayList<CanProperty> canRx = new ArrayList<>();
        canRx.add(new CanProperty(CarVendorExtensionManager.VENDOR_CANRX_ALARM_STATUS, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, "Alarm Status",
                getAlarmStatusDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCarVendorExtensionManager.getProperty(Integer.class, CarVendorExtensionManager.VENDOR_CANRX_ALARM_STATUS, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        canRx.add(new CanProperty(CarVendorExtensionManager.VENDOR_CANRX_VEHICLE_STATUS, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, "Vehicle Status",
                getVehicleStatusDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCarVendorExtensionManager.getProperty(Integer.class, CarVendorExtensionManager.VENDOR_CANRX_VEHICLE_STATUS, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        canRx.add(new CanProperty(CarVendorExtensionManager.VENDOR_CANRX_REAR_GEAR_ENGAGED, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, "Rear Gear Engaged",
                getRearGearEngagedDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCarVendorExtensionManager.getProperty(Integer.class, CarVendorExtensionManager.VENDOR_CANRX_REAR_GEAR_ENGAGED, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        canRx.add(new CanProperty(CarVendorExtensionManager.VENDOR_CANRX_PARKING_BRAKE_ON, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, "Parking Brake On",
                getParkingBrakeDescription((int)
                        ((Supplier<Object>) () -> {
                            try {
                                return mCarVendorExtensionManager.getProperty(Integer.class, CarVendorExtensionManager.VENDOR_CANRX_PARKING_BRAKE_ON, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL);
                            } catch (Exception ex) {
                                return 0;
                            }
                        }).get())));

        canRx.add(new CanProperty(CarVendorExtensionManager.VENDOR_CANRX_SAS, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, "Trigonometric sense rotation",
                ((Supplier<Object>) () -> {
                    try {
                        return mCarVendorExtensionManager.getProperty(Integer.class, CarVendorExtensionManager.VENDOR_CANRX_SAS, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL);
                    } catch (Exception ex) {
                        return 0;
                    }
                }).get()));

        canRx.add(new CanProperty(CarVendorExtensionManager.VENDOR_CANRX_SAS_OFFSET, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL, "Counter trigonometric sense rotation",
                ((Supplier<Object>) () -> {
                    try {
                        return mCarVendorExtensionManager.getProperty(Integer.class, CarVendorExtensionManager.VENDOR_CANRX_SAS_OFFSET, VehicleAreaType.VEHICLE_AREA_TYPE_GLOBAL);
                    } catch (Exception ex) {
                        return 0;
                    }
                }).get()));

        mCanPropertyList.put("CAN RX", canRx);

        //
        listPropertyAdapter = new ListPropertyAdapter(getLayoutInflater(), mCanPropertyList);
        listPropertyGroups.setAdapter(listPropertyAdapter);

        // Register callbacks
        if (car != null) {
            try {
                mSensorManager.registerListener(mOnSensorChangedListener, CarSensorManager.SENSOR_TYPE_CAR_SPEED, CarSensorManager.SENSOR_RATE_NORMAL);
                mSensorManager.registerListener(mOnSensorChangedListener, CarSensorManager.SENSOR_TYPE_FUEL_LEVEL, CarSensorManager.SENSOR_RATE_NORMAL);
                mSensorManager.registerListener(mOnSensorChangedListener, CarSensorManager.SENSOR_TYPE_FUEL_LEVEL_LOW, CarSensorManager.SENSOR_RATE_NORMAL);

                mCabinManager.registerCallback(mCabinCallback);

                mHvacManager.registerCallback(mHvacCallback);

                mCarVendorExtensionManager.registerCallback(mCarVendorExtensionCallback);
            } catch (Exception ex) {
                Utils.log(ex.toString());
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            mCarVendorExtensionManager.unregisterCallback(mCarVendorExtensionCallback);
            mHvacManager.unregisterCallback(mHvacCallback);
            mCabinManager.unregisterCallback(mCabinCallback);
            mSensorManager.unregisterListener(mOnSensorChangedListener);
        } catch (Exception ex) {
            Utils.log(ex.toString());
        }
    }
}
