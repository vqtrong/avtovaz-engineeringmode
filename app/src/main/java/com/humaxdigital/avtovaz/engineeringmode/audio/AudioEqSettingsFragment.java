package com.humaxdigital.avtovaz.engineeringmode.audio;

import android.car.Car;
import android.car.CarNotConnectedException;
import android.car.hardware.CarPropertyValue;
import android.car.hardware.CarVendorExtensionManager;
import android.content.Context;
import android.hardware.automotive.vehicle.V2_0.VehicleProperty;
import android.hardware.automotive.vehicle.V2_0.VendorAudioId;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

public class AudioEqSettingsFragment extends Fragment {
    private CarVendorExtensionManager carVendorExtensionManager;
    //    private Switch swEqBoost;
    private Switch swEqFlat;
    private Switch swEqLoudness;

    public AudioEqSettingsFragment() {
        // Required empty public constructor
    }

    static AudioEqSettingsFragment newInstance() {
        return new AudioEqSettingsFragment();
    }

    private static CarVendorExtensionManager.CarVendorExtensionCallback carVendorExtCallback = new CarVendorExtensionManager.CarVendorExtensionCallback() {
        @Override
        public void onChangeEvent(CarPropertyValue carPropertyValue) {
//            int propertyId = carPropertyValue.getPropertyId();
//            Utils.log("" + propertyId);
//
//            if(propertyId == VehicleProperty.VENDOR_AUDIO_EVENT) {
//                int eventId = carPropertyValue.getEventId();
//                if (eventId == VendorAudioId.ID_EVT_LASTSETUP) {
//                    Utils.log("got last settings");
//                }
//            }
        }

        @Override
        public void onErrorEvent(int i, int i1) {

        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_audio_eq_settings, container, false);

        //
//        swEqBoost = view.findViewById(R.id.swEqBoost);
//        swEqBoost.setOnClickListener(v -> setEqBoost(((Switch) v).isChecked()));

        //
        swEqFlat = view.findViewById(R.id.swEqFlat);
        swEqFlat.setOnClickListener(v -> setEqFlat(((Switch) v).isChecked()));

        //
        swEqLoudness = view.findViewById(R.id.swEqLoudness);
        swEqLoudness.setOnClickListener(v -> setEqLoudness(((Switch) v).isChecked()));

        try {
            carVendorExtensionManager = (CarVendorExtensionManager) App.getInstance().getCar().getCarManager(Car.VENDOR_EXTENSION_SERVICE);
            carVendorExtensionManager.registerCallback(carVendorExtCallback);
        } catch (CarNotConnectedException e) {
            Utils.log(e.toString());
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // read current settings and update UI
        getLastSettings();
    }

    @Override
    public void onDestroy() {
        if (carVendorExtensionManager != null) {
            try {
                carVendorExtensionManager.unregisterCallback(carVendorExtCallback);
            } catch (CarNotConnectedException e) {
                Utils.log(e.toString());
            }
        }
        super.onDestroy();
    }

    private void sendCommand(int commandId, int dataLength, byte[] data) {
        byte[] fullData = new byte[8 + dataLength];

        //  Add commandId
        fullData[0] = (byte) ((commandId >> 24) & 0x000000FF);
        fullData[1] = (byte) ((commandId >> 16) & 0x000000FF);
        fullData[2] = (byte) ((commandId >> 8) & 0x000000FF);
        fullData[3] = (byte) (commandId & 0x000000FF);

        if (dataLength > 0 && data != null) {
            // Add dataLength
            fullData[4] = (byte) (((dataLength) >> 24) & 0x000000FF);
            fullData[5] = (byte) (((dataLength) >> 16) & 0x000000FF);
            fullData[6] = (byte) (((dataLength) >> 8) & 0x000000FF);
            fullData[7] = (byte) ((dataLength) & 0x000000FF);

            // Add data
            System.arraycopy(data, 0, fullData, 8, dataLength);
        }

        try {
            if (carVendorExtensionManager != null) {
                carVendorExtensionManager.setGlobalProperty(byte[].class, VehicleProperty.VENDOR_AUDIO_CMD, fullData);
            }
        } catch (CarNotConnectedException e) {
            Utils.log(e.toString());
        }
    }

//    private void setEqBoost(boolean on) {
//        sendCommand(VendorAudioId.ID_CMD_TUNINGEQ, 1, new byte[]{(byte) (on ? 0x33 : 0x32)});
//    }

    private void setEqFlat(boolean on) {
        sendCommand(VendorAudioId.ID_CMD_FLATEQ, 1, new byte[]{(byte) (on ? 0x01 : 0x00)});
    }

    private void setEqLoudness(boolean on) {
        sendCommand(VendorAudioId.ID_CMD_LOUDNESS, 2, new byte[]{0x01, (byte) (on ? 0x01 : 0x00)});
    }

    private void getLastSettings() {
//        sendCommand(VendorAudioId.ID_CMD_LASTSETUP, 1, new byte[]{0x01});

        AudioManager mAudioManager;
        mAudioManager = (AudioManager) App.getInstance().getSystemService(Context.AUDIO_SERVICE);

        if (mAudioManager != null) {
            String key = "getLastSetup";
            String value = mAudioManager.getParameters(key);
            Utils.log(value); //getLastSetup=data:000102030405060708090A0B0C0D
            try {
                byte l = (byte) Integer.parseInt(value.substring(28, 30), 16);
                Utils.log("loundless = " + l);
                setEqLoudness(l == 1);
            } catch (Exception e) {
                Utils.log(e.toString());
            }
        }
    }
}
