package com.humaxdigital.avtovaz.engineeringmode.audio;

import android.annotation.SuppressLint;
import android.car.Car;
import android.car.CarNotConnectedException;
import android.car.media.CarAudioManager;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

public class AudioFaderBalanceFragment extends Fragment {

    private CarAudioManager mCarAudioManager;
    private byte fade = 0;
    private byte balance = 0;

    private TextView txtFade;
    private TextView txtBalance;

    public AudioFaderBalanceFragment() {
        // Required empty public constructor
    }

    static AudioFaderBalanceFragment newInstance() {
        return new AudioFaderBalanceFragment();
    }

    @SuppressLint("DefaultLocale")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_audio_fader_balance, container, false);

        txtFade = view.findViewById(R.id.txtFade);
        txtBalance = view.findViewById(R.id.txtBalance);

        view.findViewById(R.id.btnFadeDn).setOnClickListener(v -> {
            if (fade > -12) {
                setFade((byte) (fade - 1));
            }
        });

        view.findViewById(R.id.btnFadeUp).setOnClickListener(v -> {
            if (fade < 12) {
                setFade((byte) (fade + 1));
            }
        });

        view.findViewById(R.id.btnBalanceDn).setOnClickListener(v -> {
            if (balance > -12) {
                setBalance((byte) (balance - 1));
            }
        });

        view.findViewById(R.id.btnBalanceUp).setOnClickListener(v -> {
            if (balance < 12) {
                setBalance((byte) (balance + 1));
            }
        });


        try {
            mCarAudioManager = (CarAudioManager) App.getInstance().getCar().getCarManager(Car.AUDIO_SERVICE);
        } catch (CarNotConnectedException e) {
            Utils.log(e.toString());
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // read current settings and update UI
        getLastSettings();
    }

    @SuppressLint("DefaultLocale")
    private void setFade(byte f) {
        fade = f;
        txtFade.setText(String.format("%.1f", fade * 0.1f));
        if (mCarAudioManager != null) {
            try {
                mCarAudioManager.setFadeTowardFront(fade * 0.1f);
            } catch (CarNotConnectedException e) {
                Utils.log(e.toString());
            }
        }
    }

    @SuppressLint("DefaultLocale")
    private void setBalance(byte b) {
        balance = b;
        txtBalance.setText(String.format("%.1f", balance * 0.1f));
        if (mCarAudioManager != null) {
            try {
                mCarAudioManager.setBalanceTowardRight(balance * 0.1f);
            } catch (CarNotConnectedException e) {
                Utils.log(e.toString());
            }
        }
    }

    private void getLastSettings() {
        AudioManager mAudioManager;
        mAudioManager = (AudioManager) App.getInstance().getSystemService(Context.AUDIO_SERVICE);

        if (mAudioManager != null) {
            String key = "getLastSetup";
            String value = mAudioManager.getParameters(key);
            Utils.log(value); //getLastSetup=data:000102030405060708090A0B0C0D

            try {
                byte f = (byte) Integer.parseInt(value.substring(24, 26), 16);
                Utils.log("fade = " + f);
//                if (fade != f) {
                    setFade(f);
//                }
            } catch (Exception e) {
                Utils.log(e.toString());
            }

            try {
                byte b = (byte) Integer.parseInt(value.substring(26, 28), 16);
                Utils.log("balance = " + b);
//                if (balance != b) {
                    setBalance(b);
//                }
            } catch (Exception e) {
                Utils.log(e.toString());
            }
        }
    }
}
