package com.humaxdigital.avtovaz.engineeringmode;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.UserManager;
import android.provider.Settings;
import android.util.Log;

import androidx.core.content.FileProvider;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

public class Utils {
    private static final String TAG = "EngineeringMode";
    private static final boolean debug = true;

    public static void log(String s) {
        if (debug) {
            String[] msg = trace(Thread.currentThread().getStackTrace(), 3);
            if (msg != null) {
                Log.i(TAG, msg[0] + "  " + msg[1] + "  " + s);
            } else {
                Log.i(TAG, s);
            }
        }
    }

    private static String[] trace(final StackTraceElement[] e, final int level) {
        if (e != null && e.length >= level) {
            final StackTraceElement s = e[level];
            if (s != null) {
                return new String[]{
                        e[level].getFileName(),
                        e[level].getMethodName() + "[" + e[level].getLineNumber() + "]"
                };
            }
        }
        return null;
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        for (byte aByte : bytes) {
            int intVal = aByte & 0xff;
            if (intVal < 0x10) sbuf.append("0");
            sbuf.append(Integer.toHexString(intVal).toUpperCase());
        }
        return sbuf.toString();
    }

    public static byte[] getUTF8Bytes(String str) {
        try {
            return str.getBytes(StandardCharsets.UTF_8);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String loadFileAsString(String filename) throws java.io.IOException {
        final int BUFFER_LENGTH = 1024;
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(filename), BUFFER_LENGTH)) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFFER_LENGTH);
            byte[] bytes = new byte[BUFFER_LENGTH];
            boolean isUTF8 = false;
            int read, count = 0;
            while ((read = is.read(bytes)) != -1) {
                if (count == 0 && bytes[0] == (byte) 0xEF && bytes[1] == (byte) 0xBB && bytes[2] == (byte) 0xBF) {
                    isUTF8 = true;
                    baos.write(bytes, 3, read - 3); // drop UTF8 bom marker
                } else {
                    baos.write(bytes, 0, read);
                }
                count += read;
            }
            return isUTF8 ? new String(baos.toByteArray(), StandardCharsets.UTF_8) : new String(baos.toByteArray());
        }
    }

    public static String getSystemProperty(String propertyName) {
        String propertyValue = "null";

        try {
            Process getPropProcess = Runtime.getRuntime().exec("getprop" + " " + propertyName);
            BufferedReader osRes = new BufferedReader(new InputStreamReader(getPropProcess.getInputStream()));
            propertyValue = osRes.readLine();
            osRes.close();
        } catch (Exception e) {
            // Do nothing - can't get property value
        }

        log(propertyName + ": " + propertyValue);
        return propertyValue;
    }

    public static String getMACAddress(String interfaceName) {

        if ((new File("/sys/class/net/" + interfaceName)).exists()) {
            try {
                List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface inf : interfaces) {
                    if (interfaceName != null) {
                        if (!inf.getName().equalsIgnoreCase(interfaceName)) continue;
                    }
                    byte[] mac = inf.getHardwareAddress();
                    if (mac != null) {
                        StringBuilder buf = new StringBuilder();
                        for (byte aMac : mac) buf.append(String.format("%02X:", aMac));
                        if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                        return buf.toString();
                    }
                    return "<Error>";
                }
            } catch (Exception ex) {
                Utils.log(ex.toString());
                return "<Error>";
            } // for now eat exceptions
        }
        return "<Wifi is OFF>";
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
    }

    public static boolean isAppInstalled(String packageName) {
        try {
            App.getInstance().getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    public static void launchApp(String packageName) {
        Intent intent = new Intent();
        intent.setPackage(packageName);

        PackageManager pm = App.getInstance().getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        Collections.sort(resolveInfos, new ResolveInfo.DisplayNameComparator(pm));

        if (resolveInfos.size() > 0) {
            ResolveInfo launchable = resolveInfos.get(0);
            ActivityInfo activity = launchable.activityInfo;
            ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);

            Intent i = new Intent(Intent.ACTION_MAIN);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            i.setComponent(name);

            App.getInstance().startActivity(i);
        }
    }

    public static void setDevelopmentSettingsEnabled(boolean enable) {
        Context context = App.getInstance().getApplicationContext();
        Settings.Global.putInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, enable ? 1 : 0);
        context.sendBroadcast(new Intent("com.android.settingslib.development.DevelopmentSettingsEnabler.SETTINGS_CHANGED"));
    }

    public static boolean isDevelopmentSettingsEnabled() {
        final Context context = App.getInstance().getApplicationContext();
        final UserManager um = (UserManager) context.getSystemService(Context.USER_SERVICE);
        final boolean settingEnabled = Settings.Global.getInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, Build.TYPE.equals("eng") ? 1 : 0) != 0;
        final boolean hasRestriction = um.hasUserRestriction(UserManager.DISALLOW_DEBUGGING_FEATURES);
        final boolean isAdminOrDemo = um.isAdminUser() || um.isDemoUser();
        return isAdminOrDemo && !hasRestriction && settingEnabled;
    }

    public static void setAdbEnabled(boolean enable) {
        Settings.Global.putInt(App.getInstance().getApplicationContext().getContentResolver(), Settings.Global.ADB_ENABLED, enable ? 1 : 0);
    }

    public static boolean isAdbEnabled() {
        return Settings.Global.getInt(App.getInstance().getApplicationContext().getContentResolver(), Settings.Global.ADB_ENABLED, 0) != 0;
    }

    public static void installApk(String apkname) {
        File apkFile = new File("/mnt/sdcard/" + apkname + ".apk");
        if (apkFile.exists()) {
            apkFile.delete();
        }

        try {
            int length = 0;
            apkFile.createNewFile();
            InputStream inputStream = App.getInstance().getApplicationContext().getAssets().open(apkname + ".apk");
            log("size: " + inputStream.available());

            FileOutputStream fOutputStream = new FileOutputStream(apkFile);
            byte[] buffer = new byte[inputStream.available()];
            while ((length = inputStream.read(buffer)) > 0) {
                log("exporting: " + length);
                fOutputStream.write(buffer, 0, length);
            }
            fOutputStream.flush();
            fOutputStream.close();
            inputStream.close();
        } catch (Exception ex) {
            log(ex.toString());
        }

        installApkUsingUri(apkFile);
    }

    private static void installApkUsingUri(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        try {
            App.getInstance().startActivity(intent);
        } catch (Exception ex) {
            log(ex.toString());
        }
    }

    private void installApkUsingProvider(File file) {
        Uri apk = FileProvider.getUriForFile(App.getInstance().getApplicationContext(), "com.humaxdigital.avtovaz.engineeringmode.provider", file);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(apk, "application/vnd.android.package-archive");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        List<ResolveInfo> resInfoList = App.getInstance().getApplicationContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_ALL);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            log(packageName);
            App.getInstance().getApplicationContext().grantUriPermission(packageName, apk, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }

        try {
            App.getInstance().startActivity(intent);
        } catch (Exception ex) {
            log(ex.toString());
        }
    }
}
