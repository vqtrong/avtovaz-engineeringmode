package com.humaxdigital.avtovaz.engineeringmode;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemProperties;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.humaxdigital.avtovaz.engineeringmode.audio.AudioModeActivity;
import com.humaxdigital.avtovaz.engineeringmode.bluetooth.BluetoothModeActivity;
import com.humaxdigital.avtovaz.engineeringmode.can.CanInfoActivity;
import com.humaxdigital.avtovaz.engineeringmode.did.DidConfigsActivity;
import com.humaxdigital.avtovaz.engineeringmode.dtc.DtcModeActivity;
import com.humaxdigital.avtovaz.engineeringmode.eth.EthernetModeActivity;
import com.humaxdigital.avtovaz.engineeringmode.gps.NmeaParserActivity;
import com.humaxdigital.avtovaz.engineeringmode.gps.NmeaSentencesActivity;
import com.humaxdigital.avtovaz.engineeringmode.lcd.LcdColorsActivity;
import com.humaxdigital.avtovaz.engineeringmode.lcd.LcdWhiteActivity;
import com.humaxdigital.avtovaz.engineeringmode.radio.RadioActivity;
import com.humaxdigital.avtovaz.engineeringmode.wifi.WifiModeActivity;

public class Modules extends AppCompatActivity {
    private final int MAX_PAGES = 2;
    private int currentPage = 1;

    private String[] strMenuItems;
    private String[] strItemTitle;

    private ListView listMenu;
    private ListView listDetail;

    private TextView txtPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modules);

        listMenu = findViewById(R.id.listMenu);
        listMenu.setOnItemClickListener((adapterView, view, i, l) -> showDetail(strMenuItems[i]));

        listDetail = findViewById(R.id.listDetail);
        listDetail.setOnItemClickListener((adapterView, view, i, l) -> runAction(strItemTitle[i]));

        txtPage = findViewById(R.id.txtPage);
        Button btnPrev = findViewById(R.id.btnPrev);
        btnPrev.setOnClickListener(view -> {
            if (currentPage > 1) {
                currentPage--;
            }
            showPage(currentPage);
        });

        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(view -> {
            if (currentPage < MAX_PAGES) {
                currentPage++;
            }
            showPage(currentPage);
        });

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        // show default
        showPage(currentPage);
    }

    private void showPage(int index) {
        txtPage.setText(getString(R.string.STR_PAGE_INDEX, index, MAX_PAGES));
        if (index == 2) {
            strMenuItems = getResources().getStringArray(R.array.modules_menu_page_two);
        } else {
            strMenuItems = getResources().getStringArray(R.array.modules_menu_page_one);
        }
        listMenu.setAdapter(new ListMenuAdapter(this.getLayoutInflater(), strMenuItems, true, false));

        // show default
        listMenu.setItemChecked(0, true);
        showDetail(strMenuItems[0]);
    }

    private void showDetail(String title) {
        if (title.equals(getString(R.string.STR_DTC_MODE))) {
            queryDtcMode();
        } else if (title.equals(getString(R.string.STR_LCD))) {
            queryLcdActions();
        } else if (title.equals(getString(R.string.STR_BLUETOOTH))) {
            queryBluetooth();
            runAction(getString(R.string.STR_BLUETOOTH_MODE));
        } else if (title.equals(getString(R.string.STR_WIFI))) {
            queryWifi();
            runAction(getString(R.string.STR_WIFI_MODE));
        } else if (title.equals(getString(R.string.STR_GPS))) {
            queryGPS();
        } else if (title.equals(getString(R.string.STR_RADIO))) {
            queryRadio();
        } else if (title.equals(getString(R.string.STR_AUDIO))) {
            queryAudio();
            runAction(getString(R.string.STR_AUDIO_MODE));
        } else if (title.equals(getString(R.string.STR_USB))) {
            queryUSB();
        } else if (title.equals(getString(R.string.STR_ETHERNET))) {
            queryEthernet();
            runAction(getString(R.string.STR_ETHERNET_MODE));
        } else if (title.equals(getString(R.string.STR_CAN))) {
            queryCAN();
            runAction(getString(R.string.STR_CAN_INFO));
        } else if (title.equals(getString(R.string.STR_DID_CONFIG))) {
            queryDidConfigs();
            runAction(getString(R.string.STR_DID_CONFIG_MODE));
        }

        if (strItemTitle != null) {
            listDetail.setAdapter(new ListMenuAdapter(this.getLayoutInflater(), strItemTitle, false, true));
        }
    }

    private void runAction(String action) {
        if (action.equals(getString(R.string.STR_OPEN_DTC))) {
            Intent intent = new Intent(this, DtcModeActivity.class);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_LCD_COLORS))) {
            Intent intent = new Intent(this, LcdColorsActivity.class);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_LCD_WHITE))) {
            Intent intent = new Intent(this, LcdWhiteActivity.class);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_BLUETOOTH_MODE))) {
            Intent intent = new Intent(this, BluetoothModeActivity.class);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_WIFI_MODE))) {
            Intent intent = new Intent(this, WifiModeActivity.class);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_ETHERNET_MODE))) {
            Intent intent = new Intent(this, EthernetModeActivity.class);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_NMEA_PARSER))) {
            Intent intent = new Intent(this, NmeaParserActivity.class);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_NMEA_SENTENCES))) {
            Intent intent = new Intent(this, NmeaSentencesActivity.class);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_RADIO_FM))) {
            Intent intent = new Intent(this, RadioActivity.class);
            RadioActivity.setModeByIntent(intent, RadioActivity.RADIO_FM);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_RADIO_AM))) {
            Intent intent = new Intent(this, RadioActivity.class);
            RadioActivity.setModeByIntent(intent, RadioActivity.RADIO_AM);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_AUDIO_MODE))) {
            Intent intent = new Intent(this, AudioModeActivity.class);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_SW_UPGRADE))) {
            if (Utils.isAppInstalled("ru.yandex.yap.updater")) {
                Utils.launchApp("ru.yandex.yap.updater");
            } else {
                Toast.makeText(this, "ru.yandex.yap.updater is not installed", Toast.LENGTH_LONG).show();
            }
        } else if (action.equals(getString(R.string.STR_SW_UPGRADE_2))) {
            if (Utils.isAppInstalled("ru.yandex.yap.updater2")) {
                Utils.launchApp("ru.yandex.yap.updater2");
            } else {
                Toast.makeText(this, "ru.yandex.yap.updater2 is not installed", Toast.LENGTH_LONG).show();
            }
        } else if (action.equals(getString(R.string.STR_USB_TEST_MODE))) {
            Toast.makeText(this, "Not implemented", Toast.LENGTH_LONG).show();
        } else if (action.equals(getString(R.string.STR_CAN_INFO))) {
            Intent intent = new Intent(this, CanInfoActivity.class);
            startActivity(intent);
        } else if (action.equals(getString(R.string.STR_DID_CONFIG_MODE))) {
            Intent intent = new Intent(this, DidConfigsActivity.class);
            startActivity(intent);
        }
    }

    private void queryDtcMode() {
        strItemTitle = new String[1];

        // fill info
        strItemTitle[0] = getString(R.string.STR_OPEN_DTC);
    }

    private void queryLcdActions() {
        strItemTitle = new String[2];

        // fill info
        strItemTitle[0] = getString(R.string.STR_LCD_COLORS);
        strItemTitle[1] = getString(R.string.STR_LCD_WHITE);
    }

    private void queryBluetooth() {
        strItemTitle = new String[1];

        // fill info
        strItemTitle[0] = getString(R.string.STR_BLUETOOTH_MODE);
    }

    private void queryWifi() {
        strItemTitle = new String[1];

        // fill info
        strItemTitle[0] = getString(R.string.STR_WIFI_MODE);
    }

    private void queryGPS() {
        String drstatus = SystemProperties.get("vendor.gps.drstatus");
        String drstatusx = "";
        strItemTitle = new String[3];
        try {
            drstatusx = "(0x" + Integer.toHexString(Integer.parseInt(drstatus)) + ")";
        } catch ( Exception ex) {
            drstatusx = "";
        }
        // fill info
        strItemTitle[0] = getString(R.string.STR_DR_STATUS) + ": " + drstatus + " " + drstatusx;
        strItemTitle[1] = getString(R.string.STR_NMEA_PARSER);
        strItemTitle[2] = getString(R.string.STR_NMEA_SENTENCES);
    }

    private void queryRadio() {
        strItemTitle = new String[2];

        // fill info
        strItemTitle[0] = getString(R.string.STR_RADIO_FM);
        strItemTitle[1] = getString(R.string.STR_RADIO_AM);
    }

    private void queryAudio() {
        strItemTitle = new String[1];

        // fill info
        strItemTitle[0] = getString(R.string.STR_AUDIO_MODE);
    }

    private void queryUSB() {
        strItemTitle = new String[3];

        // fill info
        strItemTitle[0] = getString(R.string.STR_SW_UPGRADE);
        strItemTitle[1] = getString(R.string.STR_SW_UPGRADE_2);
        strItemTitle[2] = getString(R.string.STR_USB_TEST_MODE);
    }

    private void queryEthernet() {
        strItemTitle = new String[1];

        // fill info
        strItemTitle[0] = getString(R.string.STR_ETHERNET_MODE);
    }

    private void queryCAN() {
        strItemTitle = new String[1];

        // fill info
        strItemTitle[0] = getString(R.string.STR_CAN_INFO);
    }

    private void queryDidConfigs() {
        strItemTitle = new String[1];

        // fill info
        strItemTitle[0] = getString(R.string.STR_DID_CONFIG_MODE);
    }
}
