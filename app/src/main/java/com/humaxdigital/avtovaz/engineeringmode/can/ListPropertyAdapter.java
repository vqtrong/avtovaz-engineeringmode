package com.humaxdigital.avtovaz.engineeringmode.can;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.humaxdigital.avtovaz.engineeringmode.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ListPropertyAdapter extends BaseExpandableListAdapter {
    private LayoutInflater mLayoutInflater;
    private ArrayList<String> mGroups;
    private HashMap<String, ArrayList<CanProperty>> mCanPropertyList;

    ListPropertyAdapter(LayoutInflater layoutInflater, HashMap<String, ArrayList<CanProperty>> properties) {
        mLayoutInflater = layoutInflater;
        mCanPropertyList = properties;
        mGroups = new ArrayList<>(mCanPropertyList.keySet());
    }

    @Override
    public int getGroupCount() {
        return mGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<CanProperty> properties = mCanPropertyList.get(mGroups.get(groupPosition));
        if (properties != null) {
            return properties.size();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<CanProperty> properties = mCanPropertyList.get(mGroups.get(groupPosition));
        if (properties != null) {
            return properties.get(childPosition);
        }
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_group_property, null);
        }

        String groupTitle = (String) getGroup(groupPosition);
        ((TextView) convertView.findViewById(R.id.txtGroupTitle)).setText(groupTitle);

        return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item_property_detail, null);
        }

        CanProperty property = (CanProperty) getChild(groupPosition, childPosition);
        ((TextView) convertView.findViewById(R.id.txtItemTitle)).setText(property.mPropertyName);
        ((TextView) convertView.findViewById(R.id.txtItemDetail)).setText(property.mValue.toString());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
