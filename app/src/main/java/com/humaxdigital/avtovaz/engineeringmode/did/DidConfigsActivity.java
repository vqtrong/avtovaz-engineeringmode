package com.humaxdigital.avtovaz.engineeringmode.did;

import android.car.Car;
import android.car.VehicleAreaVendorExt;
import android.car.hardware.CarVendorExtensionManager;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

public class DidConfigsActivity extends AppCompatActivity {
    private CarVendorExtensionManager mCarVendorExtensionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_did_configs);

        // Connect to Car Services
        Car car = App.getInstance().getCar();
        if (car != null) {
            try {
                mCarVendorExtensionManager = (CarVendorExtensionManager) car.getCarManager(android.car.Car.VENDOR_EXTENSION_SERVICE);
            } catch (Exception ex) {
                Utils.log(ex.toString());
            }
        }
    }

    public void onConfigMode(View view) {
        if (mCarVendorExtensionManager != null) {
            try {
                mCarVendorExtensionManager.setProperty(Integer.class, CarVendorExtensionManager.VENDOR_SYSTEM_SETUP_TYPE_INT, VehicleAreaVendorExt.SETUP_CONFIG, 0);
            } catch (Exception ex) {
                Utils.log(ex.toString());
            }
        }
    }

    public void onDeliveryMode(View view) {
        if (mCarVendorExtensionManager != null) {
            try {
                mCarVendorExtensionManager.setProperty(Integer.class, CarVendorExtensionManager.VENDOR_SYSTEM_SETUP_TYPE_INT, VehicleAreaVendorExt.SETUP_DELIVERY, 0);
            } catch (Exception ex) {
                Utils.log(ex.toString());
            }
        }
    }
}
