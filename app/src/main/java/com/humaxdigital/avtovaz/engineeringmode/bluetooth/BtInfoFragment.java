package com.humaxdigital.avtovaz.engineeringmode.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemProperties;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

import java.util.Set;

public class BtInfoFragment extends Fragment {
    private static String BT_CONFIG_FILE = "/data/misc/bluedroid/bt_config.conf";

    private TextView txtMacAddr;
    private TextView txtDeviceName;
    private TextView txtLinkKey;
    private TextView txtLinkKeyContent;

    private BluetoothAdapter bluetoothAdapter;

    public BtInfoFragment() {
        // Required empty public constructor
    }

    static BtInfoFragment newInstance() {
        return new BtInfoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bt_info, container, false);

        txtMacAddr = view.findViewById(R.id.txtMacAddr);
        txtDeviceName = view.findViewById(R.id.txtDeviceName);
        txtLinkKey = view.findViewById(R.id.txtLinkKey);
        txtLinkKeyContent = view.findViewById(R.id.txtLinkKeyContent);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        txtMacAddr.setText(SystemProperties.get("persist.service.bdroid.bdaddr"));

        if (bluetoothAdapter != null) {
            txtDeviceName.setText(bluetoothAdapter.getName());
        }

        if (bluetoothAdapter != null) {
            Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                // There are paired devices. Get the name and address of each paired device.
                for (BluetoothDevice device : pairedDevices) {
                    Utils.log("Found: " + device.getName() + " at " + device.getAddress());
                }
            }
        }

        try {
            Utils.log(BT_CONFIG_FILE + "\n" + Utils.loadFileAsString(BT_CONFIG_FILE));
            txtLinkKeyContent.setText(Utils.loadFileAsString(BT_CONFIG_FILE));
        } catch (Exception ex) {
            Utils.log(ex.toString());
        }

        if (Utils.isAppInstalled("com.humaxdigital.avtovaz.btlinkkeyviewer")) {
            txtLinkKey.setText(getString(R.string.STR_OPEN_BT_LINKKEY));
            txtLinkKey.setTextColor(Color.BLUE);
            txtLinkKey.setOnClickListener(v -> Utils.launchApp("com.humaxdigital.avtovaz.btlinkkeyviewer"));
        } else {
            txtLinkKey.setText(getString(R.string.STR_INSTALL_BT_LINKKEY));
            txtLinkKey.setTextColor(Color.RED);
            txtLinkKey.setOnClickListener(v -> Utils.installApk("btlinkkeyviewer"));
        }
    }
}
