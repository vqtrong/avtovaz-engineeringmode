package com.humaxdigital.avtovaz.engineeringmode.gps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.LocationManager;
import android.location.OnNmeaMessageListener;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.gps.nmea.GnssSatellite;
import com.humaxdigital.avtovaz.engineeringmode.gps.nmea.GnssStatus;
import com.humaxdigital.avtovaz.engineeringmode.gps.nmea.NmeaParser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class NmeaParserActivity extends AppCompatActivity implements OnNmeaMessageListener {
    private TextView txtNmeaParserGPSInfo;
    private TextView txtNmeaParserSatellites;
    private NmeaParser nmeaParser;
    private GnssStatus gnssStatus;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nmea_parser);

        txtNmeaParserGPSInfo = findViewById(R.id.txtNmeaParserGPSInfo);
        txtNmeaParserGPSInfo.setText(getString(R.string.STR_WAITING_FOR_GPS));

        txtNmeaParserSatellites = findViewById(R.id.txtNmeaParserSatellites);
        txtNmeaParserSatellites.setText(getString(R.string.STR_WAITING_FOR_GPS));

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        nmeaParser = new NmeaParser();
        gnssStatus = nmeaParser.getGnssStatus();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            locationManager.addNmeaListener(this);
        }
    }

    @Override
    protected void onDestroy() {
        if (locationManager != null) {
            locationManager.removeNmeaListener(this);
        }
        super.onDestroy();
    }

    @Override
    public void onNmeaMessage(String s, long l) {
        nmeaParser.parseNmeaSentence(s);
        updateGPSInfo();
        updateSatellites();
    }

    @SuppressLint("DefaultLocale")
    private void updateGPSInfo() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("GPS Status:    %s\n", gnssStatus.getFixMode() == 3 ? "3D Fix" : (gnssStatus.getFixMode() == 2 ? "2D Fix" : "No Fix")));
        stringBuilder.append(String.format("Latitude:      %3.5f\n", gnssStatus.getLatitude()));
        stringBuilder.append(String.format("Longitude:     %3.5f\n", gnssStatus.getLongitude()));
        stringBuilder.append(String.format("Altitude:      %3.5f\n", gnssStatus.getAltitude()));
        stringBuilder.append(String.format("TTFF:          %d\n", TimeUnit.MILLISECONDS.toSeconds(gnssStatus.getTTFF())));
        stringBuilder.append(String.format("Date:          %s\n", new SimpleDateFormat("yyyy/dd/MM", Locale.US).format(new Date(gnssStatus.getTimestamp()))));
        stringBuilder.append(String.format("Time:          %s\n", new SimpleDateFormat("HH:mm:ss", Locale.US).format(new Date(gnssStatus.getTimestamp()))));
        stringBuilder.append(String.format("PDOP:          %3.3f\n", gnssStatus.getPDOP()));
        stringBuilder.append(String.format("HDOP:          %3.3f\n", gnssStatus.getHDOP()));
        stringBuilder.append(String.format("VDOP:          %3.3f\n", gnssStatus.getVDOP()));
        stringBuilder.append(String.format("Heading:       %3.3f\n", gnssStatus.getBearing()));
        stringBuilder.append(String.format("Satellites:    %d\n", gnssStatus.getNbSat()));
        stringBuilder.append(String.format("Mode:          %s\n", gnssStatus.getMode()));
        stringBuilder.append(String.format("Quality:       %d\n", gnssStatus.getQuality()));
        txtNmeaParserGPSInfo.setText(stringBuilder.toString());
    }

    @SuppressLint("DefaultLocale")
    private void updateSatellites() {
        StringBuilder stringBuilder = new StringBuilder();
        ArrayList<GnssSatellite> satellites = gnssStatus.getSatellitesList();
        ArrayList<Integer> satellitesInFix = gnssStatus.getTrackedSatellites();
        stringBuilder.append("No.     SNR   In-use\n");
        for (GnssSatellite sat : satellites) {
            if (sat.getSnr() > 0f) {
                stringBuilder.append(String.format("%3d     %3.0f     %s\n",
                        sat.getRpn(),
                        sat.getSnr(),
                        satellitesInFix.contains(sat.getRpn()) ? "Y" : "  N"));
            }
        }
        txtNmeaParserSatellites.setText(stringBuilder.toString());
    }
}
