package com.humaxdigital.avtovaz.engineeringmode.audio;

import android.annotation.SuppressLint;
import android.car.Car;
import android.car.CarNotConnectedException;
import android.car.hardware.CarPropertyValue;
import android.car.hardware.CarVendorExtensionManager;
import android.content.Context;
import android.hardware.automotive.vehicle.V2_0.VehicleProperty;
import android.hardware.automotive.vehicle.V2_0.VendorAudioId;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;


public class AudioSVDCFragment extends Fragment {

    private CarVendorExtensionManager carVendorExtensionManager;
    private byte level = 0;

    private RadioButton opt_off;
    private RadioButton opt_low;
    private RadioButton opt_medium;
    private RadioButton opt_high;

    public AudioSVDCFragment() {
        // Required empty public constructor
    }

    static AudioSVDCFragment newInstance() {
        return new AudioSVDCFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_audio_svdc, container, false);

        opt_off = view.findViewById(R.id.opt_off);
        opt_off.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setLevel((byte) 0);
            }
        });

        opt_low = view.findViewById(R.id.opt_low);
        opt_low.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setLevel((byte) 1);
            }
        });

        opt_medium = view.findViewById(R.id.opt_medium);
        opt_medium.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setLevel((byte) 2);
            }
        });

        opt_high = view.findViewById(R.id.opt_high);
        opt_high.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setLevel((byte) 3);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // read current settings and update UI
        getLastSettings();
    }

    private void sendCommand(int commandId, int dataLength, byte[] data) {
        byte[] fullData = new byte[8 + dataLength];

        //  Add commandId
        fullData[0] = (byte) ((commandId >> 24) & 0x000000FF);
        fullData[1] = (byte) ((commandId >> 16) & 0x000000FF);
        fullData[2] = (byte) ((commandId >> 8) & 0x000000FF);
        fullData[3] = (byte) (commandId & 0x000000FF);

        if (dataLength > 0 && data != null) {
            // Add dataLength
            fullData[4] = (byte) (((dataLength) >> 24) & 0x000000FF);
            fullData[5] = (byte) (((dataLength) >> 16) & 0x000000FF);
            fullData[6] = (byte) (((dataLength) >> 8) & 0x000000FF);
            fullData[7] = (byte) ((dataLength) & 0x000000FF);

            // Add data
            System.arraycopy(data, 0, fullData, 8, dataLength);
        }

        try {
            if (carVendorExtensionManager != null) {
                carVendorExtensionManager.setGlobalProperty(byte[].class, VehicleProperty.VENDOR_AUDIO_CMD, fullData);
            }
        } catch (CarNotConnectedException e) {
            Utils.log(e.toString());
        }
    }

    @SuppressLint("DefaultLocale")
    private void setLevel(byte l) {
        level = l;
        switch (level) {
            case 0:
                if (!opt_off.isChecked()) opt_off.setChecked(true);
                break;
            case 1:
                if (!opt_low.isChecked()) opt_low.setChecked(true);
                break;
            case 2:
                if (!opt_medium.isChecked()) opt_medium.setChecked(true);
                break;
            case 3:
                if (!opt_high.isChecked()) opt_high.setChecked(true);
                break;
        }
        sendCommand(VendorAudioId.ID_CMD_SDVC, 1, new byte[]{level});
    }

    private void getLastSettings() {
        AudioManager mAudioManager;
        mAudioManager = (AudioManager) App.getInstance().getSystemService(Context.AUDIO_SERVICE);

        if (mAudioManager != null) {
            String key = "getLastSetup";
            String value = mAudioManager.getParameters(key);
            Utils.log(value); //getLastSetup=data:000102030405060708090A0B0C0D
            try {
                byte l = (byte) Integer.parseInt(value.substring(34, 36), 16);
                Utils.log("level = " + l);
//                if (level != l) {
                setLevel(l);
//                }
            } catch (Exception e) {
                Utils.log(e.toString());
            }
        }
    }
}
