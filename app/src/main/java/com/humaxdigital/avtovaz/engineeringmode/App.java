package com.humaxdigital.avtovaz.engineeringmode;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Application;
import android.car.Car;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.StrictMode;

public class App extends Application {
    public static final String[] wantedPermissions = {
            Manifest.permission.ACCESS_BROADCAST_RADIO
    };

    // store application instance
    // don't bother the warning, because this class instance is only bond to Application once, and lives along with application
    @SuppressLint("StaticFieldLeak")
    private static App sInstance;

    public static synchronized App getInstance() {
        return sInstance;
    }

    private Car mCar;

    public Car getCar() {
        return mCar;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // save instance
        sInstance = this;

        Utils.log("[AvtoVAZ] Engineering version: " + BuildConfig.BUILD_TYPE + " " + BuildConfig.VERSION_NAME + " started. ");
        Utils.log("Root access: " + ExecuteAsRootBase.canRunRootCommands());
        initCarService();

        // due to an issue of Android O, an system app cannot expose provider https://blog.csdn.net/hanhan1016/article/details/79421570
        // we have to disable exposing URI checker, https://stackoverflow.com/questions/38200282/android-os-fileuriexposedexception-file-storage-emulated-0-test-txt-exposed
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    private void initCarService() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_AUTOMOTIVE)) {
            mCar = Car.createCar(this, carServiceConnection);
            if (mCar != null) {
                mCar.connect();
            }
        } else {
            Utils.log("No FEATURE_AUTOMOTIVE found!!!");
        }
    }

    private final ServiceConnection carServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Utils.log("Connected to CarServices");
            Utils.log("" + mCar + " " + mCar.isConnected());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Utils.log("Disconnected to CarServices");
            mCar = null;
        }
    };
}
