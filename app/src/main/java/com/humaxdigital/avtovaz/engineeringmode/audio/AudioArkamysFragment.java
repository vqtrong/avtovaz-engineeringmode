package com.humaxdigital.avtovaz.engineeringmode.audio;

import android.annotation.SuppressLint;
import android.car.CarNotConnectedException;
import android.car.hardware.CarVendorExtensionManager;
import android.content.Context;
import android.hardware.automotive.vehicle.V2_0.VehicleProperty;
import android.hardware.automotive.vehicle.V2_0.VendorAudioId;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;


public class AudioArkamysFragment extends Fragment {

    private CarVendorExtensionManager carVendorExtensionManager;
    private byte option = 0;

    private RadioButton opt_natural;
    private RadioButton opt_lounge;
    private RadioButton opt_live;
    private RadioButton opt_club;
    private RadioButton opt_stage;
    private RadioButton opt_surround;
    private RadioButton opt_bass_boost_off;
    private RadioButton opt_bass_boost_on;

    public AudioArkamysFragment() {
        // Required empty public constructor
    }

    static AudioArkamysFragment newInstance() {
        return new AudioArkamysFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_audio_arkamys, container, false);

        opt_natural = view.findViewById(R.id.opt_natural);
        opt_natural.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setOption((byte) 0);
            }
        });

        opt_lounge = view.findViewById(R.id.opt_lounge);
        opt_lounge.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setOption((byte) 1);
            }
        });

        opt_live = view.findViewById(R.id.opt_live);
        opt_live.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setOption((byte) 2);
            }
        });

        opt_club = view.findViewById(R.id.opt_club);
        opt_club.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setOption((byte) 3);
            }
        });

        opt_stage = view.findViewById(R.id.opt_stage);
        opt_stage.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setOption((byte) 0x10);
            }
        });

        opt_surround = view.findViewById(R.id.opt_surround);
        opt_surround.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setOption((byte) 0x11);
            }
        });

        opt_bass_boost_off = view.findViewById(R.id.opt_bass_boost_off);
        opt_bass_boost_off.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setOption((byte) 0x20);
            }
        });

        opt_bass_boost_on = view.findViewById(R.id.opt_bass_boost_on);
        opt_bass_boost_on.setOnCheckedChangeListener((c, b) -> {
            if (b) {
                setOption((byte) 0x21);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // read current settings and update UI
        getLastSettings();
    }

    private void sendCommand(int commandId, int dataLength, byte[] data) {
        byte[] fullData = new byte[8 + dataLength];

        //  Add commandId
        fullData[0] = (byte) ((commandId >> 24) & 0x000000FF);
        fullData[1] = (byte) ((commandId >> 16) & 0x000000FF);
        fullData[2] = (byte) ((commandId >> 8) & 0x000000FF);
        fullData[3] = (byte) (commandId & 0x000000FF);

        if (dataLength > 0 && data != null) {
            // Add dataLength
            fullData[4] = (byte) (((dataLength) >> 24) & 0x000000FF);
            fullData[5] = (byte) (((dataLength) >> 16) & 0x000000FF);
            fullData[6] = (byte) (((dataLength) >> 8) & 0x000000FF);
            fullData[7] = (byte) ((dataLength) & 0x000000FF);

            // Add data
            System.arraycopy(data, 0, fullData, 8, dataLength);
        }

        try {
            if (carVendorExtensionManager != null) {
                carVendorExtensionManager.setGlobalProperty(byte[].class, VehicleProperty.VENDOR_AUDIO_CMD, fullData);
            }
        } catch (CarNotConnectedException e) {
            Utils.log(e.toString());
        }
    }

    @SuppressLint("DefaultLocale")
    private void setOption(byte l) {
        option = l;
        switch (option) {
            case 0x00:
                if (!opt_natural.isChecked()) opt_natural.setChecked(true);
                break;
            case 0x01:
                if (!opt_lounge.isChecked()) opt_lounge.setChecked(true);
                break;
            case 0x02:
                if (!opt_live.isChecked()) opt_live.setChecked(true);
                break;
            case 0x03:
                if (!opt_club.isChecked()) opt_club.setChecked(true);
                break;
            case 0x10:
                if (!opt_stage.isChecked()) opt_stage.setChecked(true);
                break;
            case 0x11:
                if (!opt_surround.isChecked()) opt_surround.setChecked(true);
                break;
            case 0x20:
                if (!opt_bass_boost_off.isChecked()) opt_bass_boost_off.setChecked(true);
                break;
            case 0x21:
                if (!opt_bass_boost_on.isChecked()) opt_bass_boost_on.setChecked(true);
                break;
        }
        sendCommand(VendorAudioId.ID_CMD_TUNINGEQ, 1, new byte[]{option});
    }

    private void getLastSettings() {
        AudioManager mAudioManager;
        mAudioManager = (AudioManager) App.getInstance().getSystemService(Context.AUDIO_SERVICE);

        if (mAudioManager != null) {
            String key = "getLastSetup";
            String value = mAudioManager.getParameters(key);
            Utils.log(value); //getLastSetup=data:000102030405060708090A0B0C0D
            try {
                byte o = (byte) Integer.parseInt(value.substring(32, 34), 16);
                Utils.log("option = " + o);
//                if (option != o) {
                setOption(o);
//                }
            } catch (Exception e) {
                Utils.log(e.toString());
            }
        }
    }
}