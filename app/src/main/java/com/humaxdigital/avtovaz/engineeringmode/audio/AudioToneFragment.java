package com.humaxdigital.avtovaz.engineeringmode.audio;

import android.annotation.SuppressLint;
import android.car.Car;
import android.car.CarNotConnectedException;
import android.car.hardware.CarPropertyValue;
import android.car.hardware.CarVendorExtensionManager;
import android.content.Context;
import android.hardware.automotive.vehicle.V2_0.VehicleProperty;
import android.hardware.automotive.vehicle.V2_0.VendorAudioId;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;


public class AudioToneFragment extends Fragment {

    private CarVendorExtensionManager carVendorExtensionManager;
    private byte bass = 0;
    private byte middle = 0;
    private byte treble = 0;

    private TextView txtBass;
    private TextView txtMiddle;
    private TextView txtTreble;

    public AudioToneFragment() {
        // Required empty public constructor
    }

    static AudioToneFragment newInstance() {
        return new AudioToneFragment();
    }

    private static CarVendorExtensionManager.CarVendorExtensionCallback carVendorExtCallback = new CarVendorExtensionManager.CarVendorExtensionCallback() {
        @Override
        public void onChangeEvent(CarPropertyValue carPropertyValue) {
//            int propertyId = carPropertyValue.getPropertyId();
//            Utils.log("" + propertyId);
//
//            if(propertyId == VehicleProperty.VENDOR_AUDIO_EVENT) {
//                int eventId = carPropertyValue.getEventId();
//                if (eventId == VendorAudioId.ID_EVT_LASTSETUP) {
//                    Utils.log("got last settings");
//                }
//            }
        }

        @Override
        public void onErrorEvent(int i, int i1) {

        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_audio_tone, container, false);

        txtBass = view.findViewById(R.id.txtBass);
        txtMiddle = view.findViewById(R.id.txtMiddle);
        txtTreble = view.findViewById(R.id.txtTreble);

        view.findViewById(R.id.btnBassDn).setOnClickListener(v -> {
            if (bass > -9) {
                setBass((byte) (bass - 1));
            }
        });

        view.findViewById(R.id.btnBassUp).setOnClickListener(v -> {
            if (bass < 9) {
                setBass((byte) (bass + 1));
            }
        });

        view.findViewById(R.id.btnMiddleDn).setOnClickListener(v -> {
            if (middle > -9) {
                setMiddle((byte) (middle - 1));
            }
        });

        view.findViewById(R.id.btnMiddleUp).setOnClickListener(v -> {
            if (middle < 9) {
                setMiddle((byte) (middle + 1));
            }
        });

        view.findViewById(R.id.btnTrebleDn).setOnClickListener(v -> {
            if (treble > -9) {
                setTreble((byte) (treble - 1));
            }
        });

        view.findViewById(R.id.btnTrebleUp).setOnClickListener(v -> {
            if (treble < 9) {
                setTreble((byte) (treble + 1));
            }
        });

        try {
            carVendorExtensionManager = (CarVendorExtensionManager) App.getInstance().getCar().getCarManager(Car.VENDOR_EXTENSION_SERVICE);
            carVendorExtensionManager.registerCallback(carVendorExtCallback);
        } catch (CarNotConnectedException e) {
            Utils.log(e.toString());
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // read current settings and update UI
        getLastSettings();
    }

    private void sendCommand(int commandId, int dataLength, byte[] data) {
        byte[] fullData = new byte[8 + dataLength];

        //  Add commandId
        fullData[0] = (byte) ((commandId >> 24) & 0x000000FF);
        fullData[1] = (byte) ((commandId >> 16) & 0x000000FF);
        fullData[2] = (byte) ((commandId >> 8) & 0x000000FF);
        fullData[3] = (byte) (commandId & 0x000000FF);

        if (dataLength > 0 && data != null) {
            // Add dataLength
            fullData[4] = (byte) (((dataLength) >> 24) & 0x000000FF);
            fullData[5] = (byte) (((dataLength) >> 16) & 0x000000FF);
            fullData[6] = (byte) (((dataLength) >> 8) & 0x000000FF);
            fullData[7] = (byte) ((dataLength) & 0x000000FF);

            // Add data
            System.arraycopy(data, 0, fullData, 8, dataLength);
        }

        try {
            if (carVendorExtensionManager != null) {
                carVendorExtensionManager.setGlobalProperty(byte[].class, VehicleProperty.VENDOR_AUDIO_CMD, fullData);
            }
        } catch (CarNotConnectedException e) {
            Utils.log(e.toString());
        }
    }

    @SuppressLint("DefaultLocale")
    private void setBass(byte i) {
        bass = i;
        txtBass.setText(String.format("%d", bass));
        sendCommand(VendorAudioId.ID_CMD_TONE, 3, new byte[]{0x01, 0x00, bass});
    }

    @SuppressLint("DefaultLocale")
    private void setMiddle(byte i) {
        middle = i;
        txtMiddle.setText(String.format("%d", middle));
        sendCommand(VendorAudioId.ID_CMD_TONE, 3, new byte[]{0x01, 0x01, middle});
    }

    @SuppressLint("DefaultLocale")
    private void setTreble(byte i) {
        treble = i;
        txtTreble.setText(String.format("%d", treble));
        sendCommand(VendorAudioId.ID_CMD_TONE, 3, new byte[]{0x01, 0x02, treble});
    }

    private void getLastSettings() {
//        sendCommand(VendorAudioId.ID_CMD_LASTSETUP, 1, new byte[]{0x01});

        AudioManager mAudioManager;
        mAudioManager = (AudioManager) App.getInstance().getSystemService(Context.AUDIO_SERVICE);

        if (mAudioManager != null) {
            String key = "getLastSetup";
            String value = mAudioManager.getParameters(key);
            Utils.log(value); //getLastSetup=data:000102030405060708090A0B0C0D
            try {
                byte b = (byte) Integer.parseInt(value.substring(18, 20), 16);
                Utils.log("bass = " + b);
//                if (bass != b) {
                    setBass(b);
//                }
            } catch (Exception e) {
                Utils.log(e.toString());
            }

            try {
                byte m = (byte) Integer.parseInt(value.substring(20, 22), 16);
                Utils.log("middle = " + m);
//                if (middle != m) {
                    setMiddle(m);
//                }
            } catch (Exception e) {
                Utils.log(e.toString());
            }

            try {
                byte t = (byte) Integer.parseInt(value.substring(22, 24), 16);
                Utils.log("treble = " + t);
//                if (treble != t) {
                    setTreble(t);
//                }
            } catch (Exception e) {
                Utils.log(e.toString());
            }
        }
    }
}
