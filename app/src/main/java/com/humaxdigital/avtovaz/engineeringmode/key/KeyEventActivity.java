package com.humaxdigital.avtovaz.engineeringmode.key;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.humaxdigital.avtovaz.engineeringmode.R;

public class KeyEventActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key);
    }

    public void startMonitoring(View view) {
        Intent intent = new Intent(this, KeyEventService.class);
        startService(intent);
    }

    public void stopMonitoring(View view) {
        Intent intent = new Intent(this, KeyEventService.class);
        stopService(intent);
    }
}
