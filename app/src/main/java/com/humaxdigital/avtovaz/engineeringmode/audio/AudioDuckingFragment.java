package com.humaxdigital.avtovaz.engineeringmode.audio;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.SystemProperties;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

public class AudioDuckingFragment extends Fragment {
    private TextView txt_ducking;
    private SeekBar sld_ducking;

    public AudioDuckingFragment() {
        // Required empty public constructor
    }

    static AudioDuckingFragment newInstance() {
        return new AudioDuckingFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_audio_ducking, container, false);

        txt_ducking = view.findViewById(R.id.txt_ducking);
        sld_ducking = view.findViewById(R.id.sld_ducking);
        sld_ducking.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                setLevel(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        setLevel(0);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getLastSettings();
    }

    @SuppressLint("DefaultLocale")
    private void setLevel(int i) {
        if (txt_ducking != null) {
            txt_ducking.setText(String.format("%d dB", i));
        }
        AudioManager mAudioManager;
        mAudioManager = (AudioManager) App.getInstance().getSystemService(Context.AUDIO_SERVICE);

        if (mAudioManager != null) {
            mAudioManager.setParameters(String.format("Ducking_Sound=%d", i));
        }
    }

    private void getLastSettings() {
        AudioManager mAudioManager;
        mAudioManager = (AudioManager) App.getInstance().getSystemService(Context.AUDIO_SERVICE);
        if (mAudioManager != null) {
            String key = "Ducking_Sound";
            String value = mAudioManager.getParameters(key);
            Utils.log(value);
        }
    }
}
