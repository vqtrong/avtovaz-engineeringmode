package com.humaxdigital.avtovaz.engineeringmode.wifi;

import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.os.SystemProperties;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.SystemVersion;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

public class WifiInfoFragment extends Fragment {
    private TextView txtInterface;
    private TextView txtMacAddr;
    private TextView txtIpAddr;

    private BluetoothAdapter bluetoothAdapter;

    public WifiInfoFragment() {
        // Required empty public constructor
    }

    static WifiInfoFragment newInstance() {
        return new WifiInfoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wifi_info, container, false);

        txtInterface = view.findViewById(R.id.txtInterface);
        txtInterface.setText(SystemProperties.get("wifi.interface"));

        txtMacAddr = view.findViewById(R.id.txtMacAddr);
        txtMacAddr.setText(Utils.getMACAddress(txtInterface.getText().toString()));

        txtIpAddr = view.findViewById(R.id.txtIpAddr);
        txtIpAddr.setText(Utils.getIPAddress(true));

        return view;
    }
}