package com.humaxdigital.avtovaz.engineeringmode;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListMenuAdapter extends BaseAdapter {
    private LayoutInflater mLayoutInflater;
    private String[] mMenuItems;
    private int mItemViewId;
    private boolean mHasAction;

    public ListMenuAdapter(LayoutInflater layoutInflater, String[] menuItems, boolean selectable, boolean hasAction) {
        mLayoutInflater = layoutInflater;
        mMenuItems = menuItems;
        if (selectable) {
            mItemViewId = R.layout.list_item_menu_selectable;
        } else {
            mItemViewId = R.layout.list_item_menu;
        }
        mHasAction = hasAction;
    }

    @Override
    public int getCount() {
        return mMenuItems.length;
    }

    @Override
    public Object getItem(int i) {
        return mMenuItems[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mLayoutInflater.inflate(mItemViewId, null);
        }
        ((TextView) view.findViewById(R.id.txtItemTitle)).setText(mMenuItems[i]);
        if (mHasAction) {
            view.findViewById(R.id.iconAction).setVisibility(View.VISIBLE);
        }
        return view;
    }
}
