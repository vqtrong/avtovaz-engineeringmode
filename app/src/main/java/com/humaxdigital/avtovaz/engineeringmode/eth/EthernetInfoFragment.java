package com.humaxdigital.avtovaz.engineeringmode.eth;

import android.os.Bundle;
import android.os.SystemProperties;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

public class EthernetInfoFragment extends Fragment {
    private Switch swAdbEth;

    public EthernetInfoFragment() {
        // Required empty public constructor
    }

    static EthernetInfoFragment newInstance() {
        return new EthernetInfoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ethernet_info, container, false);

        //
        swAdbEth = view.findViewById(R.id.swAdbEth);
        swAdbEth.setOnClickListener(v -> {
            try {
                SystemProperties.set("service.adb.tcp.port", swAdbEth.isChecked() ? "5555" : "");
                SystemProperties.set("ctl.restart", "adbd");
            } catch (Exception ex) {
                Utils.log(ex.toString());
                Toast.makeText(App.getInstance().getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
            }
            swAdbEth.setChecked(SystemProperties.get("service.adb.tcp.port", "").equals("5555"));
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        swAdbEth.setChecked(SystemProperties.get("service.adb.tcp.port", "").equals("5555"));
    }
}