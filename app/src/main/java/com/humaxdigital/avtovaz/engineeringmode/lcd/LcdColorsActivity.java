package com.humaxdigital.avtovaz.engineeringmode.lcd;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.humaxdigital.avtovaz.engineeringmode.R;

public class LcdColorsActivity extends AppCompatActivity {
    private LinearLayout viewMain;
    private ImageView imgFull;
    private View decorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(flags);
        decorView.setOnSystemUiVisibilityChangeListener(visibility -> {
            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                decorView.setSystemUiVisibility(flags);
            }
        });

        setContentView(R.layout.activity_lcd_colors);

        viewMain = findViewById(R.id.viewMain);
        imgFull = findViewById(R.id.imgFull);

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());
    }

    public void onColorBlack(View view) {
        viewMain.setVisibility(View.GONE);
        imgFull.setVisibility(View.VISIBLE);
        imgFull.setBackgroundColor(Color.BLACK);
    }

    public void onColorWhite(View view) {
        viewMain.setVisibility(View.GONE);
        imgFull.setVisibility(View.VISIBLE);
        imgFull.setBackgroundColor(Color.WHITE);
    }

    public void onColorRed(View view) {
        viewMain.setVisibility(View.GONE);
        imgFull.setVisibility(View.VISIBLE);
        imgFull.setBackgroundColor(Color.RED);
    }

    public void onColorGreen(View view) {
        viewMain.setVisibility(View.GONE);
        imgFull.setVisibility(View.VISIBLE);
        imgFull.setBackgroundColor(Color.GREEN);
    }

    public void onColorBlue(View view) {
        viewMain.setVisibility(View.GONE);
        imgFull.setVisibility(View.VISIBLE);
        imgFull.setBackgroundColor(Color.BLUE);
    }

    public void onColorYellow(View view) {
        viewMain.setVisibility(View.GONE);
        imgFull.setVisibility(View.VISIBLE);
        imgFull.setBackgroundColor(Color.YELLOW);
    }

    public void onExitFull(View view) {
        viewMain.setVisibility(View.VISIBLE);
        imgFull.setVisibility(View.GONE);
        imgFull.setBackgroundColor(Color.TRANSPARENT);
    }
}
