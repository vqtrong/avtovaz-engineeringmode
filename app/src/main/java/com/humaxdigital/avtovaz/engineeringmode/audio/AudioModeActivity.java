package com.humaxdigital.avtovaz.engineeringmode.audio;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.humaxdigital.avtovaz.engineeringmode.ListMenuAdapter;
import com.humaxdigital.avtovaz.engineeringmode.R;

public class AudioModeActivity extends AppCompatActivity {
    private String[] strMenuItems;

    private ListView listMenu;
    private AudioRecordingFragment audioRecordingFragment;
    private AudioEqSettingsFragment audioEqSettingsFragment;
    private AudioToneFragment audioToneFragment;
    private AudioFaderBalanceFragment audioFaderBalanceFragment;
    private AudioSVDCFragment audioSVDCFragment;
    private AudioArkamysFragment audioArkamysFragment;
    private AudioDuckingFragment audioDuckingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_mode);

        strMenuItems = getResources().getStringArray(R.array.audio_mode_menu);

        listMenu = findViewById(R.id.listMenu);
        listMenu.setAdapter(new ListMenuAdapter(this.getLayoutInflater(), strMenuItems, true, false));
        listMenu.setOnItemClickListener((adapterView, view, i, l) -> showDetail(strMenuItems[i]));

        audioRecordingFragment = AudioRecordingFragment.newInstance();
        audioEqSettingsFragment = AudioEqSettingsFragment.newInstance();
        audioToneFragment = AudioToneFragment.newInstance();
        audioFaderBalanceFragment = AudioFaderBalanceFragment.newInstance();
        audioSVDCFragment = AudioSVDCFragment.newInstance();
        audioArkamysFragment = AudioArkamysFragment.newInstance();
        audioDuckingFragment = AudioDuckingFragment.newInstance();

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        // show default
        listMenu.setItemChecked(0, true);
        showDetail(strMenuItems[0]);
    }

    private void showDetail(String title) {
        if (title.equals(getString(R.string.STR_AUDIO_RECORDING))) {
            replaceFragment(audioRecordingFragment);
        } else if (title.equals(getString(R.string.STR_AUDIO_EQ_SETTINGS))) {
            replaceFragment(audioEqSettingsFragment);
        } else if (title.equals(getString(R.string.STR_AUDIO_TONE))) {
            replaceFragment(audioToneFragment);
        } else if (title.equals(getString(R.string.STR_AUDIO_FADER_BALANCE))) {
            replaceFragment(audioFaderBalanceFragment);
        } else if (title.equals(getString(R.string.STR_AUDIO_ARKAMYS))) {
            replaceFragment(audioArkamysFragment);
        } else if (title.equals(getString(R.string.STR_AUDIO_SDVC))) {
            replaceFragment(audioSVDCFragment);
        } else if (title.equals(getString(R.string.STR_AUDIO_DUCKING))) {
            replaceFragment(audioDuckingFragment);
        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragDetail, fragment);
        fragmentTransaction.commit();
    }
}
