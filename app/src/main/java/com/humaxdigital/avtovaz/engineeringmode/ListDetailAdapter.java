package com.humaxdigital.avtovaz.engineeringmode;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListDetailAdapter extends BaseAdapter {
    private LayoutInflater mLayoutInflater;
    private String[] mMenuItems;
    private String[] mDetailItems;

    public ListDetailAdapter(LayoutInflater layoutInflater, String[] menuItems, String[] detailItems) {
        mLayoutInflater = layoutInflater;
        mMenuItems = menuItems;
        mDetailItems = detailItems;
    }

    @Override
    public int getCount() {
        return mMenuItems.length;
    }

    @Override
    public Object getItem(int i) {
        return mMenuItems[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.list_item_detail, null);
        }
        ((TextView) view.findViewById(R.id.txtItemTitle)).setText(mMenuItems[i]);
        ((TextView) view.findViewById(R.id.txtItemDetail)).setText(mDetailItems[i]);
        return view;
    }
}
