package com.humaxdigital.avtovaz.engineeringmode;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.humaxdigital.avtovaz.engineeringmode.key.KeyEventActivity;

import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class HomeActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private String[] strMenuItems;
    private ListView listMenu;
    Switch swAdb;
    Switch swDevMode;
    private int hiddenTouchCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        findViewById(R.id.txtTitle).setOnClickListener(v -> {
            hiddenTouchCount++;
            if (hiddenTouchCount >= 10) {
                swAdb.setVisibility(View.VISIBLE);
                swDevMode.setVisibility(View.VISIBLE);
                makeMenu(true);
                hiddenTouchCount = 0;
            }
        });

        swAdb = findViewById(R.id.swAdb);
        swAdb.setOnClickListener(v -> Utils.setAdbEnabled(((Switch) v).isChecked()));

        swDevMode = findViewById(R.id.swDevMode);
        swDevMode.setOnClickListener(v -> Utils.setDevelopmentSettingsEnabled(((Switch) v).isChecked()));

        listMenu = findViewById(R.id.listMenu);
        listMenu.setOnItemClickListener((adapterView, view, i, l) -> gotoPage(strMenuItems[i]));
        makeMenu(false);

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        // check permission
        if (!EasyPermissions.hasPermissions(this, App.wantedPermissions)) {
            Utils.log("MISSING PERMISSIONS NEED TO BE GRANT MANUALLY");
            EasyPermissions.requestPermissions(this, "This app needs some permissions!", 0xBEEFBEEF, App.wantedPermissions);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        swAdb.setChecked(Utils.isAdbEnabled());
        swDevMode.setChecked(Utils.isDevelopmentSettingsEnabled());
    }

    private void makeMenu(boolean hasHidden) {
        strMenuItems = getResources().getStringArray(hasHidden ? R.array.list_home_menu_with_hidden : R.array.list_home_menu);
        for (int i = 0; i < strMenuItems.length; i++) {
            if (strMenuItems[i].startsWith(getString(R.string.STR_APP_VERSION))) {
                strMenuItems[i] = String.format("%s: %s_%s",
                        getString(R.string.STR_APP_VERSION),
                        BuildConfig.BUILD_TYPE,
                        BuildConfig.VERSION_NAME);
            }
        }
        listMenu.setAdapter(new ListMenuAdapter(this.getLayoutInflater(), strMenuItems, false, false));
    }

    private void gotoPage(String title) {
        if (title.equals(getString(R.string.STR_SYSTEM_VERSION))) {
            Intent intent = new Intent(this, SystemVersion.class);
            startActivity(intent);
        } else if (title.equals(getString(R.string.STR_MODULES))) {
            Intent intent = new Intent(this, Modules.class);
            startActivity(intent);
        } else if (title.equals(getString(R.string.STR_KEY_EVENT))) {
            Intent intent = new Intent(this, KeyEventActivity.class);
            startActivity(intent);
        } else if (title.equals(getString(R.string.STR_SYSTEM_SETTINGS))) {
            Intent intent = new Intent("android.settings.SETTINGS");
            startActivity(intent);
        } else if (title.equals(getString(R.string.STR_DEV_SETTINGS))) {
            Intent intent = new Intent("android.settings.APPLICATION_DEVELOPMENT_SETTINGS");
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Utils.log(Integer.toHexString(requestCode) + ":" + permissions.length + ":" + grantResults.length);
        for (String str : permissions) {
            Utils.log(str);
        }

        for (int i : grantResults) {
            Utils.log("" + i);
        }

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Utils.log(Integer.toHexString(requestCode) + ":" + perms.size());
        for (String str : perms) {
            Utils.log(str);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        Utils.log(Integer.toHexString(requestCode) + ":" + perms.size());
        for (String str : perms) {
            Utils.log(str);
        }
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this)
                    .setTitle("Permissions Required")
                    .setPositiveButton("Settings")
                    .setNegativeButton("Cancel")
                    .setRequestCode(0xB33FB33F)
                    .build()
                    .show();
        }
    }
}
