package com.humaxdigital.avtovaz.engineeringmode.key;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.car.Car;
import android.car.CarInputManager;
import android.car.CarNotConnectedException;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.WindowManager;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

public class KeyEventService extends Service {
    private WindowManager windowManager;
    private WindowManager.LayoutParams viewParams;
    private KeyEventView keyEventView;
    private CarInputManager mCarInputManager;

    private final CarInputManager.KeyListener mGlobalKeyListener = new CarInputManager.KeyListener() {
        @Override
        public boolean onKeyEvent(KeyEvent keyEvent) {
            try {
                int keyCode = keyEvent.getKeyCode();
                int keyAction = keyEvent.getAction();
                boolean longPress = keyEvent.isLongPress();
                int repeatCount = keyEvent.getRepeatCount();

                Utils.log("keyCode = " + keyCode + " keyName = " + KeyEventView.STR_KEY_CODE[keyCode] + " keyAction = " + keyAction + " longPress = " + longPress + " repeatCount = " + repeatCount);

                if (keyEventView != null) {
                    keyEventView.setKeyCode(keyCode);
                    keyEventView.setKeyAction(keyAction);
                    keyEventView.setLongPressed(longPress);
                    keyEventView.setRepeatCount(repeatCount);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    };


    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder notificationBuilder;

    private NotificationCompat.Builder getNotificationBuilder(Context context, String channelId, int importance) {
        prepareChannel(channelId, importance);
        return new NotificationCompat.Builder(context, channelId);
    }

    private void prepareChannel(String id, int importance) {
        String appName = App.class.getSimpleName();
        if (mNotificationManager != null) {
            NotificationChannel nChannel = mNotificationManager.getNotificationChannel(id);
            if (nChannel == null) {
                nChannel = new NotificationChannel(id, appName, importance);
                mNotificationManager.createNotificationChannel(nChannel);
            }
        }
    }

    public Notification getServiceNotification() {
        Context mContext = App.getInstance().getApplicationContext();
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        // from Android O, if you want to maintain service running in background when the activity is removed,
        // you have to set service as Foreground service with Notification informing user about its activities
        notificationBuilder = getNotificationBuilder(
                mContext,
                "engineering_mode_notification_channel",
                NotificationManagerCompat.IMPORTANCE_LOW //Low importance prevents visual appearance for this notification channel on top
        );

        // set notification attributes
        notificationBuilder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setCategory(Notification.CATEGORY_SERVICE);

        return notificationBuilder.build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(0xFE, getServiceNotification());

        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        viewParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);
        viewParams.x = 0;
        viewParams.y = 0;
        viewParams.gravity = Gravity.END | Gravity.TOP;

        keyEventView = new KeyEventView(this);

        windowManager.addView(keyEventView, viewParams);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(mCarInputManager == null) {
            Car mCarApi = App.getInstance().getCar();
            if (mCarApi != null && mCarApi.isConnected()) {
                try {
                    mCarInputManager = (CarInputManager) mCarApi.getCarManager(Car.CAR_INPUT_SERVICE);
                    Utils.log("registered VendorCarInput global key listener");
                } catch (CarNotConnectedException e) {
                    e.printStackTrace();
                }
                if (mCarInputManager != null) {
                    try {
                        mCarInputManager.registerGlobalKeyEventListener(mGlobalKeyListener, 10);
                        Utils.log("registered VendorCarInput global key listener");
                    } catch (CarNotConnectedException e) {
                        e.printStackTrace();
                    }
                }
            }

            keyEventView.startView();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (mCarInputManager != null) {
            try {
                mCarInputManager.unregisterGlobalKeyEventListener(mGlobalKeyListener);
                Utils.log("Unregistered VendorCarInput global key listener");
                mCarInputManager = null;
            } catch (CarNotConnectedException e) {
                e.printStackTrace();
            }
        }

        keyEventView.stopView();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}