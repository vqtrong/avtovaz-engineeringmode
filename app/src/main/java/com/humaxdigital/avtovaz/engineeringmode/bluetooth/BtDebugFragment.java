package com.humaxdigital.avtovaz.engineeringmode.bluetooth;

import android.os.Bundle;
import android.os.SystemProperties;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.humaxdigital.avtovaz.engineeringmode.App;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

public class BtDebugFragment extends Fragment {
    private static String BT_STACK_FILE = "/etc/bluetooth/bt_stack.conf";
    private static String BT_STACK_BACKUP_FILE = "/data/misc/bluetooth/bt_stack.bk";
    private static String BT_VENDOR_FILE = "/etc/bluetooth/bt_vendor.conf";
    private static String BT_SNOOP_HCI_FILE = "/data/misc/bluetooth/logs/btsnoop_hci.log";

    //    private Switch swBtStackLog;
    private Switch swBtSnoopLog;
//    private Switch swBtCallSoundCaptureIn;
//    private Switch swBtCallSoundCaptureOut;
//    private Switch swBtCallSoundCaptureCodecIn;
//    private Switch swBtCallSoundCaptureCodecOut;

    public BtDebugFragment() {
        // Required empty public constructor
    }

    static BtDebugFragment newInstance() {
        return new BtDebugFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bt_debug, container, false);

        //
//        swBtStackLog = view.findViewById(R.id.swBtStackLog);
//        swBtStackLog.setOnClickListener(v -> {
//            setBtStackLog(swBtStackLog.isChecked());
//            swBtStackLog.setChecked(isBtStackLogEnabled());
//        });

        //
        swBtSnoopLog = view.findViewById(R.id.swBtSnoopLog);
        swBtSnoopLog.setOnClickListener(v -> {
            setBtSnoopLog(swBtSnoopLog.isChecked());
            swBtSnoopLog.setChecked(SystemProperties.get("persist.bluetooth.btsnoopenable").equals("true"));
        });

        //
//        swBtCallSoundCaptureIn = view.findViewById(R.id.swBtCallSoundCaptureIn);
//        swBtCallSoundCaptureIn.setOnClickListener(v -> {
//            try {
//                SystemProperties.set("vendor.audio.hfpdump.bt_in", swBtCallSoundCaptureIn.isChecked() ? "true" : "false");
//            } catch (Exception ex) {
//                Utils.log(ex.toString());
//                Toast.makeText(App.getInstance().getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
//            }
//        swBtCallSoundCaptureIn.setChecked(SystemProperties.get("vendor.audio.hfpdump.bt_in").equals("true"));
//        });

        //
//        swBtCallSoundCaptureOut = view.findViewById(R.id.swBtCallSoundCaptureOut);
//        swBtCallSoundCaptureOut.setOnClickListener(v -> {
//            try {
//                SystemProperties.set("vendor.audio.hfpdump.bt_output", swBtCallSoundCaptureOut.isChecked() ? "true" : "false");
//            } catch (Exception ex) {
//                Utils.log(ex.toString());
//                Toast.makeText(App.getInstance().getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
//            }
//        swBtCallSoundCaptureOut.setChecked(SystemProperties.get("vendor.audio.hfpdump.bt_output").equals("true"));
//        });

        //
//        swBtCallSoundCaptureCodecIn = view.findViewById(R.id.swBtCallSoundCaptureCodecIn);
//        swBtCallSoundCaptureCodecIn.setOnClickListener(v -> {
//            try {
//                SystemProperties.set("vendor.audio.hfpdump.codec_in", swBtCallSoundCaptureCodecIn.isChecked() ? "true" : "false");
//            } catch (Exception ex) {
//                Utils.log(ex.toString());
//                Toast.makeText(App.getInstance().getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
//            }
//        swBtCallSoundCaptureCodecIn.setChecked(SystemProperties.get("vendor.audio.hfpdump.codec_in").equals("true"));
//        });

        //
//        swBtCallSoundCaptureCodecOut = view.findViewById(R.id.swBtCallSoundCaptureCodecOut);
//        swBtCallSoundCaptureCodecOut.setOnClickListener(v -> {
//            try {
//                SystemProperties.set("vendor.audio.hfpdump.cordec_out", swBtCallSoundCaptureCodecOut.isChecked() ? "true" : "false");
//            } catch (Exception ex) {
//                Utils.log(ex.toString());
//                Toast.makeText(App.getInstance().getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
//            }
//        swBtCallSoundCaptureCodecOut.setChecked(SystemProperties.get("vendor.audio.hfpdump.cordec_out").equals("true"));
//        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

//        swBtStackLog.setChecked(isBtStackLogEnabled());
        swBtSnoopLog.setChecked(SystemProperties.get("persist.bluetooth.btsnoopenable").equals("true"));
//        swBtCallSoundCaptureIn.setChecked(SystemProperties.get("vendor.audio.hfpdump.bt_in").equals("true"));
//        swBtCallSoundCaptureOut.setChecked(SystemProperties.get("vendor.audio.hfpdump.bt_output").equals("true"));
//        swBtCallSoundCaptureCodecIn.setChecked(SystemProperties.get("vendor.audio.hfpdump.codec_in").equals("true"));
//        swBtCallSoundCaptureCodecOut.setChecked(SystemProperties.get("vendor.audio.hfpdump.cordec_out").equals("true"));

        try {
            Utils.log(BT_STACK_FILE + "\n" + Utils.loadFileAsString(BT_STACK_FILE));
        } catch (Exception ex) {
            Utils.log(ex.toString());
        }

        try {
            Utils.log(BT_VENDOR_FILE + "\n" + Utils.loadFileAsString(BT_VENDOR_FILE));
        } catch (Exception ex) {
            Utils.log(ex.toString());
        }

        try {
            Utils.log(BT_SNOOP_HCI_FILE + "\n" + Utils.loadFileAsString(BT_SNOOP_HCI_FILE));
        } catch (Exception ex) {
            Utils.log(ex.toString());
        }
    }

//    private void setBtStackLog(boolean enable) {
//        try {
//            List<String> lines = Files.readAllLines(new File(BT_STACK_FILE).toPath(), StandardCharsets.UTF_8);
//            String[] arr = lines.toArray(new String[0]);
//
//            ArrayList<CharSequence> old_lines = new ArrayList<>();
//            ArrayList<CharSequence> new_lines = new ArrayList<>();
//
//            for (String str : arr) {
//                // remove spaces
//                String line = str.trim();
//
//                // save old line
//                old_lines.add(line);
//
//                // config settings
//                String new_line = line;
//                if (line.startsWith("TRC_")) {
//                    String[] conf = line.split("=");
//                    new_line = conf[0] + (enable ? "=6" : "=2");
//                    Utils.log("Config BT Stack: " + line + " --> " + new_line);
//
//                }
//
//                // save new line
//                new_lines.add(new_line);
//            }
//            File backup = new File(BT_STACK_BACKUP_FILE);
//            if (backup.exists()) {
//                backup.delete();
//            }
//            Files.write(backup.toPath(), old_lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
//
//            File config = new File(BT_STACK_FILE);
//            if (config.exists()) {
//                config.delete();
//            }
//            Files.write(config.toPath(), new_lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
//        } catch (Exception ex) {
//            Utils.log(ex.toString());
//            Toast.makeText(App.getInstance().getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
//        }
//    }

    private void setBtSnoopLog(boolean enable) {
        Utils.log("request = " + enable);

        try {
            SystemProperties.set("persist.bluetooth.btsnooplogmode", enable ? "full" : "disabled");
            SystemProperties.set("persist.bluetooth.btsnoopenable", enable ? "true" : "false");
            if (enable) {
                Toast.makeText(App.getInstance().getApplicationContext(), "You have to restart the device to start logging", Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            Utils.log(ex.toString());
            Toast.makeText(App.getInstance().getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
        }

        Utils.log("persist.bluetooth.btsnooplogmode = " + SystemProperties.get("persist.bluetooth.btsnooplogmode"));
        Utils.log("persist.bluetooth.btsnoopenable = " + SystemProperties.get("persist.bluetooth.btsnoopenable"));
    }

//    private boolean isBtStackLogEnabled() {
//        try {
//            List<String> lines = Files.readAllLines(new File(BT_STACK_FILE).toPath(), StandardCharsets.UTF_8);
//            String[] arr = lines.toArray(new String[0]);
//
//            for (String str : arr) {
//                String line = str.trim();
//                if (line.startsWith("TRC_")) {
//                    String[] conf = line.split("=");
//                    if (conf.length == 2) {
//                        if (!conf[1].trim().equals("6")) {
//                            Utils.log("BT Stack is not configured for " + conf[0] + ", its value is " + conf[1]);
//                            return false;
//                        }
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            Utils.log(ex.toString());
//        }
//
//        Utils.log("BT Stack Log is configured!");
//        return true;
//    }
}
