package com.humaxdigital.avtovaz.engineeringmode.radio;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.hardware.radio.ProgramSelector;
import android.hardware.radio.RadioManager;
import android.hardware.radio.RadioTuner;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RadioActivity extends AppCompatActivity {
    public static final int RADIO_FM = 0;
    public static final int RADIO_AM = 1;
    private int mode = RADIO_FM;

    private RadioManager.FmBandDescriptor mFmDescriptor;
    private RadioManager.AmBandDescriptor mAmDescriptor;
    private RadioManager.BandConfig mFmConfig;
    private RadioManager.BandConfig mAmConfig;
    private RadioTuner mRadioTuner;

    private RadioManager.ProgramInfo programInfo;
    private RadioManager.BandConfig radioConfig;

    final double[] mAMIFBW = {0.0, 2.0, 2.2, 2.4, 2.7, 3.0, 3.3, 3.6, 4.0, 4.4, 4.9, 5.4, 5.9, 6.6, 7.2, 8.0};
    final int[] mFMIFBW = {55, 72, 89, 107, 124, 141, 159, 176, 193, 211, 228, 245, 262, 280, 297, 314};

    private TextView txtRadioInfo;
    private TextView txtRadioRegisterParameter;
    private TextView txtRadioSignal;

    private Handler mSignalHandler = new Handler();
    private Runnable requestSignalThread = new Runnable() {
        public void run() {
            getSignal();
            mSignalHandler.postDelayed(this, 1000);
        }
    };

    private AudioManager mAudioManager;
    private AudioFocusRequest mAudioFocusRequest;
    private AudioAttributes mAudioAttributes;
    private AudioManager.OnAudioFocusChangeListener mAudioFocusChangeListener;
    private boolean isHasAudioFocus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mode = extras.getInt("RadioMode", RADIO_FM);
        }

        setContentView(R.layout.activity_radio);

        TextView txtTitle = findViewById(R.id.txtTitle);
        if (mode == RADIO_FM) {
            txtTitle.setText(R.string.STR_RADIO_FM);
        } else {
            txtTitle.setText(R.string.STR_RADIO_AM);
        }

        txtRadioInfo = findViewById(R.id.txtRadioInfo);
        txtRadioRegisterParameter = findViewById(R.id.txtRegisterParameter);
        txtRadioSignal = findViewById(R.id.txtSignal);

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        initAudio();
        initRadio();

        Utils.log("Radio created, mode = " + mode);
    }

    public static void setModeByIntent(Intent intent, int mode) {
        if (mode == RADIO_FM) {
            intent.putExtra("RadioMode", RadioActivity.RADIO_FM);
        } else if (mode == RADIO_AM) {
            intent.putExtra("RadioMode", RadioActivity.RADIO_AM);
        } else {
            intent.putExtra("RadioMode", RadioActivity.RADIO_FM);
        }
    }

    private void initAudio() {
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        mAudioFocusChangeListener = focusChange -> {
            Utils.log("AudioFocusChangeListener focusChange to " + focusChange);
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                Utils.log("AUDIOFOCUS_LOSS");
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                Utils.log("AUDIOFOCUS_LOSS_TRANSIENT");
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                Utils.log("AUDIOFOCUS_LOSS_TRANSIENT");
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                Utils.log("AUDIOFOCUS_GAIN");
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN_TRANSIENT) {
                Utils.log("AUDIOFOCUS_GAIN_TRANSIENT");
            }
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_LOSS:
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
//                    release_audioPatch_radio();
                    isHasAudioFocus = false;
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    break;
                case AudioManager.AUDIOFOCUS_GAIN:
                case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT:
                    requestAudioFocus();
                    isHasAudioFocus = true;
                    break;
            }
            Utils.log("audio focus changed to " + isHasAudioFocus);
        };

        mAudioAttributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
//                .addTag(AUDIO_FOCUS_TAG)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();

        mAudioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                .setAudioAttributes(mAudioAttributes)
                .setOnAudioFocusChangeListener(mAudioFocusChangeListener)
                .build();
    }

    private void initRadio() {
        RadioManager mRadioManager = (RadioManager) getSystemService(Context.RADIO_SERVICE);
        if (mRadioManager == null) {
            throw new IllegalStateException("RadioManager could not be loaded.");
        }

        List<RadioManager.ModuleProperties> mModules = new ArrayList<>();
        int status = mRadioManager.listModules(mModules);
        if (status != RadioManager.STATUS_OK) {
            throw new IllegalStateException("Load modules failed with status: " + status);
        }

        if (mModules.size() == 0) {
            throw new IllegalStateException("No radio modules on device.");
        }

        for (RadioManager.BandDescriptor band : mModules.get(0).getBands()) {
            Utils.log("Loading band: " + band.toString());

            if (mFmDescriptor == null && band.isFmBand() && band.getType() == RadioManager.BAND_FM) {
                mFmDescriptor = (RadioManager.FmBandDescriptor) band;
            }

            if (mAmDescriptor == null && band.isAmBand() && band.getType() == RadioManager.BAND_AM) {
                mAmDescriptor = (RadioManager.AmBandDescriptor) band;
            }
        }

        if (mFmDescriptor == null && mAmDescriptor == null) {
            throw new IllegalStateException("No AM and FM radio bands could be loaded.");
        }

        if (mFmDescriptor != null) /* CID 1967887 */ {
            mFmConfig = new RadioManager.FmBandConfig.Builder(mFmDescriptor)
                    .setStereo(true)
                    .build();
        }

        if (mAmDescriptor != null) /* CID 1967886 */ {
            mAmConfig = new RadioManager.AmBandConfig.Builder(mAmDescriptor)
                    .setStereo(true)
                    .build();
        }

        Utils.log("mFmConfig:" + mFmConfig);
        Utils.log("mAmConfig:" + mAmConfig);
        Utils.log("Radio start");

        if (mRadioTuner != null) {
            Utils.log("Tuner is already open!");
            mRadioTuner.close();
            mRadioTuner = null;
        }

        if (mode == RADIO_FM) {
            mRadioTuner = mRadioManager.openTuner(mModules.get(0).getId(),
                    mFmConfig, true, radioCallback, null);
        } else {
            mRadioTuner = mRadioManager.openTuner(mModules.get(0).getId(),
                    mAmConfig, true, radioCallback, null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSignalHandler.removeCallbacks(requestSignalThread);
    }

    @Override
    protected void onDestroy() {
        Utils.log("Radio destroyed, mode = " + mode);
        if (isHasAudioFocus) {
//            release_audioPatch_radio();
            releaseAudioFocus();
        }

        if (mRadioTuner != null) {
            mRadioTuner.close();
            mRadioTuner = null;
        }

        super.onDestroy();
    }

    public void requestAudioFocus() {
        int result = mAudioManager.requestAudioFocus(mAudioFocusRequest);
        Utils.log("REQUEST audio focus, result = " + result);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            isHasAudioFocus = true;
//            create_audioPatch_radio();
        }
    }

    public void releaseAudioFocus() {
        if (mAudioManager != null && mAudioFocusRequest != null) {
            int result = mAudioManager.abandonAudioFocusRequest(mAudioFocusRequest);
            isHasAudioFocus = false;
            Utils.log("release audio focus, result = " + result);
        }
    }

    private RadioTuner.Callback radioCallback = new RadioTuner.Callback() {

        @SuppressLint("SetTextI18n")
        @Override
        public void onError(int status) {
            txtRadioInfo.setText("Tuner Error");
        }

        @SuppressLint("DefaultLocale")
        @Override
        public void onTuneFailed(int result, ProgramSelector selector) {
            txtRadioInfo.setText(String.format("Tuner Failed: result = %d\n%s", result, selector.toString()));
        }

        @Override
        public void onConfigurationChanged(RadioManager.BandConfig config) {
            //Utils.log("" + config);

            radioConfig = config;
            if (mRadioTuner != null) {
                mRadioTuner.tune(config.getLowerLimit(), 0);
                getParameter();
                mSignalHandler.postDelayed(requestSignalThread, 0);
                if (!isHasAudioFocus) {
                    requestAudioFocus();
                }
            }
        }

        @Override
        public void onProgramInfoChanged(RadioManager.ProgramInfo info) {
            //Utils.log("" + info);

            programInfo = info;
            if (info.getCmd() == RadioManager.CMD_GETINFO_TUNER_REGISTER) {
                updateRegisterParameter(info);
            } else if (info.getCmd() == RadioManager.CMD_GETINFO_SIGNAL) {
                updateSignal(info);
            } else {
                updateFrequency(info);
            }
        }
    };

    @SuppressLint("DefaultLocale")
    private void updateFrequency(RadioManager.ProgramInfo info) {
        if (info != null) {
            StringBuilder stringBuilder = new StringBuilder();

            if (mode == RADIO_FM) {
                stringBuilder.append(String.format("FRQ:%.1f MHz\n", ((float) programInfo.getChannel() / 1000)));
                stringBuilder.append(String.format("MIN:%.1f MHz\n", ((float) radioConfig.getLowerLimit() / 1000)));
                stringBuilder.append(String.format("MAX:%.1f MHz\n", ((float) radioConfig.getUpperLimit() / 1000)));
            } else {
                stringBuilder.append(String.format("FRQ:%d kHz\n", programInfo.getChannel()));
                stringBuilder.append(String.format("MIN:%d kHz\n", radioConfig.getLowerLimit()));
                stringBuilder.append(String.format("MAX:%d kHz\n", radioConfig.getUpperLimit()));
            }
            txtRadioInfo.setText(stringBuilder.toString());
        }
    }

    @SuppressLint("DefaultLocale")
    private void updateSignal(RadioManager.ProgramInfo info) {
        StringBuilder stringBuilder = new StringBuilder();
        // "fs","fof","mod","usn_adj","mlt","high cut","ms","rqi","bw"

        if (mode == RADIO_FM) {
            stringBuilder.append(String.format("MOD    : %.2f kHz", (float) ((Integer.parseInt(Objects.requireNonNull(info.getVendorInfo().get("mod"))) * 75.f) / 100.0f)));
        } else {
            stringBuilder.append(String.format("MOD    : %d", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("mod")))));
            stringBuilder.append("%");
        }

        stringBuilder.append(String.format("\nLevel  : %d dBuV", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("fs")))));

        if (mode == RADIO_FM) {
            stringBuilder.append(String.format("\nUSN    : %d", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("usn_adj"))).byteValue()));
        } else {
            stringBuilder.append(String.format("\nUSN_ADJ: %d", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("usn_adj"))).byteValue()));
        }

        if (mode == RADIO_FM) {
            stringBuilder.append(String.format("\nWAM    : %d", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("mlt")))));
        }

        stringBuilder.append(String.format("\nFOF    : %d", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("fof"))).byteValue()));
        //stringBuilder.append(String.format("\nIFBW   : %d kHz", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("ms")))));

        if (mode == RADIO_FM) {
            //stringBuilder.append(String.format("\nIFBW   : %d", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("bw")))));
            if (Integer.parseInt(Objects.requireNonNull(info.getVendorInfo().get("ms"))) > 16) {
                stringBuilder.append("\nIFBW   :    kHz");
            } else {
                stringBuilder.append(String.format("\nIFBW   : %d kHz", mFMIFBW[Integer.parseInt(Objects.requireNonNull(info.getVendorInfo().get("ms")))]));
            }
        } else {
            //stringBuilder.append(String.format("\nIFBW   : %d kHz", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("bw")))));
            if (Integer.parseInt(Objects.requireNonNull(info.getVendorInfo().get("ms"))) > 16) {
                stringBuilder.append("\nIFBW   :    kHz");
            } else {
                stringBuilder.append(String.format("\nIFBW   : %.1f kHz", mAMIFBW[Integer.parseInt(Objects.requireNonNull(info.getVendorInfo().get("ms")))]));
            }
        }

        if (mode == RADIO_FM) {
            stringBuilder.append(String.format("\nRQI    : %d", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("rqi")))));
            stringBuilder.append(String.format("\nHighcut: %d ", Integer.valueOf(Objects.requireNonNull(info.getVendorInfo().get("highcut")))));
        }

        txtRadioSignal.setText(stringBuilder);
    }

    @SuppressLint("DefaultLocale")
    private void updateRegisterParameter(RadioManager.ProgramInfo info) {
        StringBuilder stringBuilder = new StringBuilder();

        int i = 0;
        String[] words = Objects.requireNonNull(info.getVendorInfo().get("data")).split(" ");

        stringBuilder.append("High level register 0x00 ~ 0x1F(32EA)");
        for (String loop : words) {
            if (i < 32) {
                if (i % 8 == 0) {
                    stringBuilder.append(String.format("\n0x%02x : ", i));
                }
                stringBuilder.append(String.format("0x%02X ", Integer.valueOf(loop)));
            } else {
                if (mode == RADIO_FM) {
                    /* Change Format Require to RF : Change "Source %2" Position -> "%1[dBuV]" and Delete "Source %2" */
                    /* additionalInfo = QString("\n\n\nFM Seek Level : %1[dBuV], Source %2").arg(QString("%1").arg((Strdata[32]-16)/2, 0, 10)).arg(Strdata[32], 0, 10) */
                    switch (i) {
                        case 33:
                            stringBuilder.append(String.format("\n\nFM Seek Level          : %d[dBuV]", Integer.valueOf(loop)));
                            break;
                        case 34:
                            stringBuilder.append(String.format("\nFM Multipath threshold : %d", Integer.valueOf(loop)));
                            stringBuilder.append("[%]");
                            break;
                        case 35:
                            stringBuilder.append(String.format("\nFM USN threshold       : %d", Integer.valueOf(loop)));
                            stringBuilder.append("[%]");
                            break;
                        case 36:
                            stringBuilder.append(String.format("\nFM FREQ offset         : %d[kHz], 0x%02X", Integer.valueOf(loop), Integer.valueOf(loop)));
                            break;
                        case 37:
                            stringBuilder.append(String.format("\nFM USM diff            : %d", Integer.valueOf(loop)));
                            break;
                        case 38:
                            stringBuilder.append(String.format("\nFM SUM Mlt USM         : %d", Integer.valueOf(loop)));
                            break;
                        case 39:
                            stringBuilder.append(String.format("\nFM RQI                 : %d", Integer.valueOf(loop)));
                            break;
                    }
                    if (i > 39) {
                        break;
                    }
                } else {
                    /* Change Format Require to RF : Change "Source %2" Position -> "%1[dBuV]" and Delete "Source %2" */
                    /* additionalInfo = QString("\n\n\nAM Seek Level : %1[dBuV], Source %2").arg(QString("%1").arg((Strdata[32]-16)/2, 0, 10)).arg(Strdata[32], 0, 10) */
                    switch (i) {
                        case 33:
                            stringBuilder.append(String.format("\n\n\nAM Seek Level   : %d[dBuV]", Integer.valueOf(loop)));
                            break;
                        case 34:
                            stringBuilder.append(String.format("\nAM ADJ threshold: %d[dB], 0x%02X", Integer.valueOf(loop).byteValue(), Integer.valueOf(loop).byteValue()));
                            break;
                        case 35:
                            stringBuilder.append(String.format("\nAM FREQ offset  : %d[kHz], 0x%02X", Integer.valueOf(loop), Integer.valueOf(loop)));
                            break;
                    }
                    if (i > 35) {
                        break;
                    }
                }
            }
            i++;
        }

        txtRadioRegisterParameter.setText(stringBuilder.toString());
    }

    private void getParameter() {
        if (mRadioTuner != null) {
            mRadioTuner.getInfo(RadioTuner.GET_INFO_TUNER_REGISTER);
        }
    }

    private void getSignal() {
        if (mRadioTuner != null) {
            mRadioTuner.getInfo(RadioTuner.GET_INFO_SIGNAL);
        }
    }

    public void onTuneUp(View view) {
        if (mRadioTuner != null) {
            mRadioTuner.step(RadioTuner.DIRECTION_UP, false);
        }
    }

    public void onTuneDown(View view) {
        if (mRadioTuner != null) {
            mRadioTuner.step(RadioTuner.DIRECTION_DOWN, false);
        }
    }

    public void onSeekUp(View view) {
        if (mRadioTuner != null) {
            mRadioTuner.scan(RadioTuner.DIRECTION_UP, false);
        }
    }

    public void onSeekDown(View view) {
        if (mRadioTuner != null) {
            mRadioTuner.scan(RadioTuner.DIRECTION_DOWN, false);
        }
    }

    public void onScan(View view) {
        if (mRadioTuner != null) {
            mRadioTuner.scan(RadioTuner.DIRECTION_SCAN, false);
        }
    }

    public void onActiveMode(View view) {
        mRadioTuner.cmd(new byte[]{0x00, 0x00});
    }

    public void onPassiveMode(View view) {
        mRadioTuner.cmd(new byte[]{0x00, 0x01});
    }
}
