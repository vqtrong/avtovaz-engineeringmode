package com.humaxdigital.avtovaz.engineeringmode.gps;

import android.content.Context;
import android.location.LocationManager;
import android.location.OnNmeaMessageListener;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.humaxdigital.avtovaz.engineeringmode.R;

public class NmeaSentencesActivity extends AppCompatActivity implements OnNmeaMessageListener {
    private TextView txtNmeaSentences;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nmea_sentences);

        txtNmeaSentences = findViewById(R.id.txtNmeaSentences);

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            locationManager.addNmeaListener(this);
        }

    }

    @Override
    protected void onDestroy() {
        if (locationManager != null) {
            locationManager.removeNmeaListener(this);
        }
        super.onDestroy();
    }

    @Override
    public void onNmeaMessage(String s, long l) {
        if (txtNmeaSentences != null) {
            txtNmeaSentences.append(s);
            if (!s.endsWith("\r\n")) {
                txtNmeaSentences.append("\r\n");
            }
        }
    }
}
