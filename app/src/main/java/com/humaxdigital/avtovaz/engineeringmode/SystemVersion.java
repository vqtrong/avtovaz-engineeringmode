package com.humaxdigital.avtovaz.engineeringmode;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemProperties;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Date;
import java.util.function.Supplier;

public class SystemVersion extends AppCompatActivity {

    private String[] strMenuItems;
    private String[] strItemTitle;
    private String[] strItemDetail;
    private ListView listMenu;
    private ListView listDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_version);

        strMenuItems = getResources().getStringArray(R.array.list_system_version_menu);

        listMenu = findViewById(R.id.listMenu);
        listMenu.setAdapter(new ListMenuAdapter(this.getLayoutInflater(), strMenuItems, true, false));
        listMenu.setOnItemClickListener((adapterView, view, i, l) -> showDetail(strMenuItems[i]));

        listDetail = findViewById(R.id.listDetail);

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        // show
        listMenu.setItemChecked(0, true);
        showDetail(strMenuItems[0]);
    }

    private void showDetail(String title) {
        if (title.equals(getString(R.string.STR_SYSTEM_VERSION))) {
            querySystemVersion();
        } else if (title.equals(getString(R.string.STR_MICOM_VERSION))) {
            queryMicomVersion();
        } else if (title.equals(getString(R.string.STR_CANDB_VERSION))) {
            queryCanDbVersion();
        } else if (title.equals(getString(R.string.STR_GPS_VERSION))) {
            queryGpsVersion();
        } else if (title.equals(getString(R.string.STR_CONNECTIVITY_VERSION))) {
            queryConnectivityVersion();
        } else if (title.equals(getString(R.string.STR_WIFI_VERSION))) {
            queryWifiVersion();
        } else if (title.equals(getString(R.string.STR_HARDWARE_VERSION))) {
            queryHardwareVersion();
        }

        if (strItemTitle != null && strItemDetail != null) {
            listDetail.setAdapter(new ListDetailAdapter(this.getLayoutInflater(), strItemTitle, strItemDetail));
        }
    }

    private void querySystemVersion() {
        strItemTitle = new String[5];
        strItemDetail = new String[5];

        // fill info
        strItemTitle[0] = getString(R.string.STR_MODEL);
        strItemDetail[0] = Build.MODEL;

        strItemTitle[1] = getString(R.string.STR_BUILD_DATE);
        strItemDetail[1] = new Date(Build.TIME).toString();

        strItemTitle[2] = getString(R.string.STR_SYSTEM_VERSION);
        strItemDetail[2] = Build.DISPLAY; //Build.FINGERPRINT;

        strItemTitle[3] = getString(R.string.STR_KERNEL_VERSION);
        strItemDetail[3] = System.getProperty("os.name") + " " + System.getProperty("os.version");

        strItemTitle[4] = getString(R.string.STR_ANDROID_VERSION);
        strItemDetail[4] = getAndroidVersionName();
    }

    private void queryMicomVersion() {
        strItemTitle = new String[2];
        strItemDetail = new String[2];

        strItemTitle[0] = getString(R.string.STR_MICOM_VERSION);
        strItemDetail[0] = SystemProperties.get("vendor.version.micom");

        strItemTitle[1] = getString(R.string.STR_BOOTLOADER_VERSION);
        strItemDetail[1] = SystemProperties.get("ro.boot.bootloader");
    }

    private void queryCanDbVersion() {
        strItemTitle = new String[2];
        strItemDetail = new String[2];

        strItemTitle[0] = getString(R.string.STR_M_CAN_A53);
        strItemDetail[0] = getString(R.string.STR_NA);

        strItemTitle[1] = getString(R.string.STR_M_CAN_R5);
        strItemDetail[1] = getString(R.string.STR_NA);
    }

    private void queryGpsVersion() {
        strItemTitle = new String[1];
        strItemDetail = new String[1];

        strItemTitle[0] = getString(R.string.STR_GPS_VERSION);
        strItemDetail[0] = SystemProperties.get("vendor.gps.gpsversion");
    }

    private void queryWifiVersion() {
        strItemTitle = new String[2];
        strItemDetail = new String[2];

        /*
        strItemTitle[0] = getString(R.string.STR_INTERFACE);
        strItemDetail[0] = Utils.getSystemProperty("wifi.interface");

        strItemTitle[1] = getString(R.string.STR_MAC_ADDRESS);
        strItemDetail[1] = Utils.getMACAddress(strItemDetail[0]);

        strItemTitle[2] = getString(R.string.STR_IP_ADDRESS);
        strItemDetail[2] = Utils.getIPAddress(true);
         */

        strItemTitle[0] = getString(R.string.STR_WIFI_DRIVER);
        strItemDetail[0] = ((Supplier<String>) () -> {
            String value = SystemProperties.get("ro.humax.wifi.driver.version");
            if (value.equals("")) {
                return "<Permission Error>";
            } else {
                return value;
            }
        }).get();

        strItemTitle[1] = getString(R.string.STR_WIFI_FIRMWARE);
        strItemDetail[1] = ((Supplier<String>) () -> {
            String value = SystemProperties.get("ro.humax.wifi.fw.version");
            if (value.equals("")) {
                return "<Permission Error>";
            } else {
                return value;
            }
        }).get();
    }

    private void queryHardwareVersion() {
        strItemTitle = new String[1];
        strItemDetail = new String[1];

        strItemTitle[0] = getString(R.string.STR_HARDWARE_VERSION);
        strItemDetail[0] = SystemProperties.get("vendor.version.hardware");
    }

    private void queryConnectivityVersion() {
        strItemTitle = new String[2];
        strItemDetail = new String[2];

        strItemTitle[0] = getString(R.string.STR_CARPLAY_VERSION);
        strItemDetail[0] = "N/A";
        try {
            PackageInfo packageInfo = App.getInstance().getApplicationContext().getPackageManager().getPackageInfo("com.humaxdigital.carplay", 0);
            strItemDetail[0] = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Utils.log(e.toString());
        }

        strItemTitle[1] = getString(R.string.STR_ANDROID_AUTO_VERSION);
        strItemDetail[1] = "N/A";
        try {
            PackageInfo packageInfo = App.getInstance().getApplicationContext().getPackageManager().getPackageInfo("com.humaxdigital.androidauto", 0);
            strItemDetail[1] = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Utils.log(e.toString());
        }
    }

    public static String getAndroidVersionName() {
        double release = Double.parseDouble(Build.VERSION.RELEASE.replaceAll("(\\d+[.]\\d+)(.*)", "$1"));
        String codeName = "Unsupported"; //below Jelly bean OR above Oreo
        if (release >= 4.1 && release < 4.4) codeName = "Jelly Bean";
        else if (release < 5) codeName = "Kit Kat";
        else if (release < 6) codeName = "Lollipop";
        else if (release < 7) codeName = "Marshmallow";
        else if (release < 8) codeName = "Nougat";
        else if (release < 9) codeName = "Oreo";
        else if (release < 10) codeName = "Pie";
        return codeName + " v" + release + ", API Level: " + Build.VERSION.SDK_INT;
    }
}
