package com.humaxdigital.avtovaz.engineeringmode.bluetooth;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.humaxdigital.avtovaz.engineeringmode.ListMenuAdapter;
import com.humaxdigital.avtovaz.engineeringmode.R;
import com.humaxdigital.avtovaz.engineeringmode.Utils;

public class BluetoothModeActivity extends AppCompatActivity {
    private String[] strMenuItems;

    private ListView listMenu;
    private BtInfoFragment btInfoFragment;
    private BtDebugFragment btDebugFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_mode);

        strMenuItems = getResources().getStringArray(R.array.bluetooth_mode_menu);

        listMenu = findViewById(R.id.listMenu);
        listMenu.setAdapter(new ListMenuAdapter(this.getLayoutInflater(), strMenuItems, true, false));
        listMenu.setOnItemClickListener((adapterView, view, i, l) -> showDetail(strMenuItems[i]));

        btInfoFragment = BtInfoFragment.newInstance();
        btDebugFragment = BtDebugFragment.newInstance();

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        // show default
        listMenu.setItemChecked(0, true);
        showDetail(strMenuItems[0]);
    }

    private void showDetail(String title) {
        if (title.equals(getString(R.string.STR_BT_INFO))) {
            replaceFragment(btInfoFragment);
        } else if (title.equals(getString(R.string.STR_BT_DEBUG))) {
            replaceFragment(btDebugFragment);
        } else if (title.equals(getString(R.string.STR_BT_DUT_MODE))) {
            if (Utils.isAppInstalled("com.humaxdigital.automotive.dutmode")) {
                Utils.launchApp("com.humaxdigital.automotive.dutmode");

                listMenu.setItemChecked(0, true);
                showDetail(strMenuItems[0]);
            } else {
//                Toast.makeText(this, "BT DUT Mode is not installed", Toast.LENGTH_LONG).show();
                Utils.installApk("dutmode");
                // show default
                listMenu.setItemChecked(0, true);
                showDetail(strMenuItems[0]);
            }
        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragDetail, fragment);
        fragmentTransaction.commit();
    }
}
